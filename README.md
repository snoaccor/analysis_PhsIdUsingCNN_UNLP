# CNN Id for ZRad photons 

## To Run locally
first make sure you have the following packages installed

* tensorflow\n
* keras
* ROOT

otherwise install them, try:
pip3 install <pkg> --user 

### [A] Training the CNN
Run ClusterAnalysis.py like this:
>   python3 ClusterAnalysis.py

The script reads the files from the Folders:
>   EGAM3_InputCNN: ROOT Output files from CellDumpingPackage for EGAM3 files.

>   xAOD_InputCNN: ROOT Output files from CellDumpingPackage for xAOD files.

You can add more files in there or change the Input folders inside ClusterAnalysis;
although I don't recommend that, unless you've read and understood the Data Flow 
procedure.

### [B] Computing the Efficiencies in selecting events as Signal.
Run ComputeEff.py like this:
> ./ComputeEff.py -f <file/s> -t <Type_of_file> -m <model_path>

the above are required!

or Run without any [OPTIONS] to see what other options you have

### Output
>   [A]

>   [B]

## Recomended workflow
    [1] Create a folder to test:
        >   mkdir testarea
    [2] Create folders to store your models architecture files,
        the .h5 files:
        >   mkdir models
        
    [3] To train add __ClusterAnalysis__, __ClusterTrain_Module__ and __CNN_Module__ to testarea.
    
    [4] To compute efficiencies add ComputeEff and ClusterTest_Module to testarea.
