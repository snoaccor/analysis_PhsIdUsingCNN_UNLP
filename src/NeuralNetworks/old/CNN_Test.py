import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import RMSprop
from tensorflow.keras.layers import Conv2D, MaxPooling2D
from tensorflow.keras.layers import Activation, Dropout, Flatten, Dense
from tensorflow.keras.callbacks import CSVLogger

TRAINING_LOGS_FILE = "training_logs.csv"
MODEL_SUMMARY_FILE = "model_summary.txt"
TEST_FILE = "test_file.txt"
MODEL_FILE = "My_model.h5"
#Paths to data
training_data_dir="data/training"
validation_data_dir="data/validation"
test_data_dir="data/validation"

# Some Parameters
image_size =200
IMAGE_WIDTH, IMAGE_HEIGHT = image_size, image_size
EPOCHS = 20
BATCH_SIZE = 32 #que es esto??: el tamano del conjunto de imagenes
test_size = 30 

# specify the dimensions of the object to be passed to the node.
# the 3 is because the image is a RGB
Input_shape=(IMAGE_WIDTH, IMAGE_HEIGHT,3)
epochs = 20

# ----- Model -----

model = tf.keras.Sequential()

model.add(Conv2D(32, 3, 3, input_shape=Input_shape, activation='relu',padding='same'))
model.add(MaxPooling2D(pool_size=(2,2)))
#model.add(Conv2D(256, 3, 3, border_mode='same', activation='relu'))
#model.add(Conv2D(256, 3, 3, border_mode='same', activation='relu'))
#model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Flatten())  # q hace esto??
model.add(Dense(256, activation='relu')) # q hace esto??
model.add(Dropout(0.5)) # q hace esto??

model.add(Dense(256, activation='relu'))
model.add(Dropout(0.5))

model.add(Dense(1))
model.add(Activation('sigmoid'))
    
model.compile(loss='binary_crossentropy',
            optimizer=RMSprop(lr=0.0001),
            metrics=['accuracy'])




# if we didnt transform jpg from RGB to greyscale then:
# tf.image.rgb_to_greyscale(<images>)
# input_shape=tf.image.rgb_to_greyscale((IMAGE_WIDTH, IMAGE_HEIGHT,3))

# ----- Data augmentation -----
# in order to use the model we need to normalize
# the value of the images.

training_data_generator = ImageDataGenerator(
    rescale=1./255,
    shear_range=0.1, # I should look up what this is for
    zoom_range=0.1,
    horizontal_flip=True)
validation_data_generator = ImageDataGenerator(rescale=1./255)
test_data_generator = ImageDataGenerator(rescale=1./255)

# ----- Data preparation -----
# very fancy way of LOADING DATA FROM DIRECTORIES

training_generator = training_data_generator.flow_from_directory(
    training_data_dir,
    target_size=(IMAGE_WIDTH, IMAGE_HEIGHT),
    batch_size=BATCH_SIZE,
    class_mode="binary")

validation_generator = validation_data_generator.flow_from_directory(
    validation_data_dir,
    target_size=(IMAGE_WIDTH, IMAGE_HEIGHT),
    batch_size=BATCH_SIZE,
    class_mode="binary")

# ----- Training -----
print('Training model')
model.fit_generator(
    training_generator, #what to use for training
    steps_per_epoch=len(training_generator.filenames) // BATCH_SIZE,
    epochs=EPOCHS,
    validation_data=validation_generator,
    validation_steps=len(validation_generator.filenames) // BATCH_SIZE,
    #callbacks=[PlotLossesKeras(), CSVLogger(TRAINING_LOGS_FILE,
    #                                        append=False,
    #                                        separator=";")],
    verbose=1)
model.save_weights(MODEL_FILE)

# model.load_weights(MODEL_FILE)
