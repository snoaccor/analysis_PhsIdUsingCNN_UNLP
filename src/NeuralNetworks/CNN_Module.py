#import matplotlib.image as mpimg
#from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.optimizers import RMSprop
from tensorflow.keras.metrics import BinaryAccuracy
from keras.layers import Conv2D, MaxPooling2D, Activation, Dropout, Flatten, Dense
from keras.callbacks import CSVLogger
from time import time
from os import mkdir
from os.path import join
from datetime import date

class MyModels(object):
    def __init__(self,name,architecture=None):
        self.name = str(name)
        if architecture !=None:
            self.architecture=architecture
        return
    
    @property
    def Set_Model_dir(self):
        try:
            # --Create directory
            mkdir(self.name)
        except FileExistsError:
            pass
        return
    
    @property
    def TrainLogs_file(self):
        path = join(self.name,str(date.today()) + '_training_logs.csv')
        return path
    
    @property
    def Summary_file(self):
        path = join(self.name,str(date.today()) + '_summary.txt')
        return path
    
    @property
    def Model_file(self):
        path = join(self.name,str(date.today()) +'_'+ self.name + '.h5')
        return path

    @property
    def History_files(self):
        path_acc = join(self.name,str(date.today()) + '_history_acc.png')
        path_loss = join(self.name,str(date.today()) + '_history_loss.png')
        return (path_acc,path_loss)

    # ---- Define NN models here ---- #
#TODO:: take the compile out of the models definition.
def model0(input_shape):
    model = Sequential()
    model.add(Conv2D(32, (3, 3), padding='same', input_shape=input_shape, activation='relu'))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Flatten())
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(1))
    model.add(Activation('sigmoid'))
    model.compile(
            loss='binary_crossentropy',
            optimizer=RMSprop(lr=0.0001),
            metrics=[BinaryAccuracy()]
                 )
                 #metrics=['accuracy'])
    return model
                                
def model5(input_shape):
    model = Sequential()
    model.add(Conv2D(32, (3, 3), padding='same', input_shape=input_shape, activation='relu'))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Conv2D(64, (3,3), padding='same', activation='relu'))
    model.add(Conv2D(64, (3,3), padding='same', activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

#     model.add(Conv2D(128, (3,3), padding='same', activation='relu'))
#     model.add(Conv2D(128, (3,3), padding='same', activation='relu'))
#     model.add(MaxPooling2D(pool_size=(2, 2)))
#     model.add(Conv2D(256, (3,3), padding='same', activation='relu'))
#     model.add(Conv2D(256, (3,3), padding='same', activation='relu'))
#     model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Flatten())
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(1))
    model.add(Activation('sigmoid'))
    
    model.compile(
            loss='binary_crossentropy',
            optimizer=RMSprop(lr=0.0001),
            metrics=[BinaryAccuracy()]
            )
    return model

def train_model(x_train, y_train,CNN_model,EPOCHS,VALIDATION_SPLIT, TRAINING_LOGS_FILE,MODEL_FILE):
    CNN_model.fit(
            x=x_train,    #use images/matrices as x_train
            y=y_train,    #use labels as y_train
            epochs=EPOCHS,
            validation_split=VALIDATION_SPLIT,    #regulate what percentage of the training batch you want for validation
            callbacks=[
                CSVLogger(TRAINING_LOGS_FILE, append=False, separator=";")
                ]
    )
    CNN_model.save(MODEL_FILE)
    return CNN_model


        
