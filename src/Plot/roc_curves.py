#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys
import argparse
import ROOT as r
import uproot as pr
import os
# import CNN_Module
from array import array

# import SwanImports

# --Global param
# default_outfilename=os.path.split(__file__)
default_outfilename=os.path.basename(__file__).split('.')[0]
default_outfilename= f'{default_outfilename}_out.root'

class Script_setup:
    """
    Description

    # Args::None}

    # Returns::
    """
    def __init__(self,output=None,output_style=None):
        # --Set parse config
        self.parser = self.config_Parse()
        self.name=os.path.basename(__file__).split('.')[0]
        if output!=None:
            self.output = output
        else:
            self.output = self.set_IO()
        # if output_style!=None:
            # assert (output_style=='CSV' or output_style=='ROOT'),'*==> enter valid output format, either "ROOT" or "CSV"'
            # self.output_style = output_style
        # else:
            # self.output_style = 'BOTH'


    def set_IO(self,outputfile=None):
        if outputfile==None:
            outputfile = f'{self.name}_out'


    def config_Parse(self):
        """
        Set all the configuration to your parser object.

        # Args::None

        # Returns::parser object.
        """
        parser = argparse.ArgumentParser('name')
        parser.add_argument('-I', '--Input', required=True, help='<Input folder or file/s>' )
        parser.add_argument('-O', '--Output', required=False, default=default_outfilename, help='<Output folder or files/s>')
        parser.add_argument('-D','--Debug', required=False, help='Debug flag', action='store_true')
        parser.add_argument('-M','--MaxEvents', required=False, help='Set   maximum of events. Default -1 == all', type=int, default=-1)
        return parser

class pyROOT_ROCCurve:
    """
    Description

    # Args::
        nbr_pts: number of points to plot,
        meaning number of threshold to consider.

    # Returns::
    """
    def __init__(self,nbr_pts=50,):
        self.nbr_pts = nbr_pts
        self.roc_TPR = set_TPR_arr()
        self.roc_FPR = set_FPR_arr()


    def set_TPR_arr():
        arrTPR = array.array('f')


def main(argv):
    m_script = Script_setup()
    args = m_script.parser.parse_args()
    # parser = config_Parse()
    # args = parser.parse_args()

    print(default_outfilename)

    # --Set I/O
    outputfile = r.TFile(args.Output,'update')
    outTree = r.TTree('roc_curve_out','roc_curve_out')
    # TODO: vvvvvv
    # outTree.Branch('tight',tight_roc,'tight/F')
    # outTree.Branch('model0',model0_roc,'model0/F')
    # outTree.Branch('model1',model1_roc,'model1/F')
    c1=r.TCanvas('c1')
    # g = r.TGraph()
    # --Load data
    data = r.TChain('eval_cnn_out')
    data.Add(args.Input)
    events = data.GetEntries()
    max_events = events


    if (args.MaxEvents)>0:
        max_events = min(events,args.MaxEvents)

    # roc = r.TMVA.ROCCurve(trueval,tarval)
    # roc = r.TMVA.ROCCurve(data.v_zrad,bool(data.v_tight))
    # --Loop chain
    # g = r.TGraph()
    # roc_TPR = array('f',[0])
    # roc_FPR = array('f',[0])
    # roc_TPR = array('f')
    # roc_FPR = array('f')

    x1 = []
    y1 = []

    # roc_TPR={'L2HLT':[],'tight':[],'model0':[],'model1':[]}
    # roc_FPR={'L2HLT':[],'tight':[],'model0':[],'model1':[]}
    roc_TPR={'L2HLT':[],'tight':[],'MC_trainCNN':[]}
    roc_FPR={'L2HLT':[],'tight':[],'MC_trainCNN':[]}

    # roc_TPR, roc_FPR = pyROOT_ROCCurve(thrld,true_val,false_val,predic_val)

    data_dir={}
    predic_val={}

    for thrld in np.arange(0,1,0.05):
        print(f'--> {thrld}')
        # tot_true = 0
        # tot_false = 0
        # count_TP = {'L2HLT':0,'tight':0,'model0':0,'model1':0}
        # count_FP = {'L2HLT':0,'tight':0,'model0':0,'model1':0}
        count_TP = {'L2HLT':0,'tight':0,'MC_trainCNN':0}
        count_TN = {'L2HLT':0,'tight':0,'MC_trainCNN':0}
        count_FP = {'L2HLT':0,'tight':0,'MC_trainCNN':0}
        count_FN = {'L2HLT':0,'tight':0,'MC_trainCNN':0}
        for ev in range(max_events):
            data.GetEntry(ev)
            trueval = data.v_zrad
            falseval = data.v_Bkg
            # NOTE: with tight I get only 1 point
            # targetval = data.v_tight
            predic_val['L2HLT'] = data.v_L2HLT
            predic_val['tight'] = data.v_tight
            # predic_val['model0'] = data.v_cnnmodel0predict
            # predic_val['model1'] = data.v_cnnmodel1predict
            predic_val['MC_trainCNN'] = data.v_cnnMCpredict
            # # --Skip bad data
            if (trueval==0. and falseval==0.): continue

            # --Set threshold
            for methkey in predic_val.keys():
                count_TP[methkey] += trueval*(predic_val[methkey]>thrld)
                count_FP[methkey] += (1 - trueval)*(predic_val[methkey]>thrld)
                count_TN[methkey] += falseval*(predic_val[methkey]<=thrld)
                count_FN[methkey] += (1 - falseval)*(predic_val[methkey]<=thrld)

            # if trueval>0:
                # tot_true+=1
                # for methkey in predic_val.keys():
                    # if predic_val[methkey]>thrld:
                    # # if targetval>threshold:
                        # count_TP[methkey]+=1

            # if falseval>0:
                # tot_false+=1
                # # if targetval>thrld:
                # for methkey in predic_val.keys():
                    # if predic_val[methkey]>thrld:
                    # # if targetval>threshold:
                        # count_FP[methkey]+=1
                        # # count_TN[methkey]+=1

    # print(f'T:{tot_true}, F:{tot_false}')
        # print(f'Tcnt = {count_TP}, Fcnt = {count_FP}')
        for methkey in roc_TPR.keys():
            # roc_TPR[methkey].append(count_TP[methkey]/tot_true)
            # roc_FPR[methkey].append(count_FP[methkey]/tot_false)
            roc_TPR[methkey].append(count_TP[methkey]/(count_TP[methkey]+count_FN[methkey]))
            roc_FPR[methkey].append(count_FP[methkey]/(count_FP[methkey]+count_TN[methkey]))

        # y1.append(count_TP/tot_true)
        # x1.append(count_FP/tot_false)
    # roc_TPR=np.asarray(roc_TPR.tolist())
    # roc_FPR=np.asarray(roc_FPR.tolist())
    outTree.Fill()

    roc_TPR = {xkey: np.asarray(roc_TPR[xkey]) for xkey in roc_TPR.keys()}
    roc_FPR = {xkey: np.asarray(roc_FPR[xkey]) for xkey in roc_FPR.keys()}

    for xkey in roc_TPR.keys():
        np.save(f'roc_TPR_{xkey}.npy',roc_TPR[xkey])
        np.save(f'roc_FPR_{xkey}.npy',roc_FPR[xkey])

    n = len(roc_FPR['tight'])

    graphs = {kk:r.TGraph(n,roc_FPR[kk],roc_TPR[kk]) for kk in roc_TPR.keys()}
    # graphs = {kk:r.TGraph(n,roc_TPR[kk],roc_FPR[kk]) for kk in roc_TPR.keys()}

    # --Save TGraphs in root file

    # g_tight = r.TGraph(n,roc_FPR['tight'],roc_TPR['tight'])
    # g_0 = r.TGraph(n,roc_FPR['model0'],roc_TPR['model0'])
    # g_1 = r.TGraph(n,roc_FPR['model1'],roc_TPR['model1'])
    # g_tight.SetMarkerColor(4)
    # g_0.SetMarkerColor(2)
    # g_1.SetMarkerColor(1)


    # legend = r.TLegend(0.15,0.55,0.2,0.5)
    legend = r.TLegend(0.65,0.60,0.8,0.75)
    # legend = r.TLegend()
    # legend = r.TLegend(0.1,0.7,0.48,0.9)
    legend.SetFillStyle(3000)
    legend.SetFillColor(0)
    legend.SetBorderSize(0)
    legend.SetTextFont(52)

    mdefault_TGraph_style = {
            'xlabel':'FPR',
            'ylabel':'TPR',
            # 'xlabel':'TPR',
            # 'ylabel':'FPR',
            'mc':2,     # color
            'msty':20,  # style
            'ms':1.,    # size
            }
    def mset_TGraph_style(TGraph,**kwargs):
        TGraph.GetXaxis().SetTitle(kwargs['xlabel'])
        TGraph.GetXaxis().SetLimits(0.0,1.1)
        TGraph.GetYaxis().SetTitle(kwargs['ylabel'])
        TGraph.GetYaxis().SetRangeUser(0.0,1.1)
        TGraph.SetMarkerColor(kwargs['mc'])
        TGraph.SetMarkerStyle(kwargs['msty'])
        TGraph.SetMarkerSize(kwargs['ms'])


    g_sty={}
    for k,g in graphs.items():
        # --Set different styles
        g_sty[k]=mdefault_TGraph_style
        # --Create TGraphs
        mset_TGraph_style(g,**g_sty[k])
        legend.AddEntry(g,k,'p')
        g_sty[k]['mc']+=1
        g_sty[k]['msty']+=10

    # --Draw
    graphs['L2HLT'].Draw('ACP')
    graphs['tight'].Draw('CP Same')
    # graphs['model0'].Draw('CP Same')
    # graphs['model1'].Draw('CP Same')
    graphs['MC_trainCNN'].Draw('CP Same')


    legend.Draw()
    c1.Update()
    c1.Draw()
    c1.SaveAs('roc_curve_out.png')
    # c1.SaveAs('roc_curve_out.root')
    c1.SaveAs('roc_curve_out.C')
    # c1.SaveAs(outputfile)

    # g.Draw("AC*")
    # g.SaveAs('test.png')

    # y = roc_TPR.tolist()
    # x = roc_FPR.tolist()
    # print(x)
    # print(y)
    # print(x1,y1)
    # plt.plot(x1,y1)
    # plt.plot(roc_FPR,roc_TPR)
    # plt.show()

    outputfile.Close()

    return

if __name__ == '__main__':
    main(sys.argv[1:])
