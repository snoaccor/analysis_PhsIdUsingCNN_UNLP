#!/usr/bin/env python3
# coding: utf-8

import ROOT as r
import sys
import argparse
from SwanImports import *
SetAtlasStyle()


# define hists array
hists = []
m_min=0.5
m_max=1.1
m_bins = 100

Colour = [1, 2, 3, 4 ]

DrawOptions = ['','Same', 'Same','Same']

legend = r.TLegend( 0.2, 0.5, 0.2, 0.5)
legend.SetFillStyle(3000)
legend.SetFillColor(0)
legend.SetBorderSize(0)
legend.SetTextFont(52)


def Magic(Data,Variable,histogram,labels,Cuts):
    for i in range(len(labels)):
        # create the histograms
        histogram.append(r.TH1F(labels[i],labels[i],m_bins,m_min,m_max))
        # Now project data on that hist
        print('Projecting...')
        Data.Project(labels[i],Variable,Cuts[i])
        print('Hist called "'+labels[i]+'" has '+ str(histogram[-1].GetEntries())+' entries')
        # Now normalizehist to unity
        histogram[-1].Scale(1./histogram[-1].Integral())
        histogram[-1].SetMarkerColor(Colour[i])
        histogram[-1].SetLineColor(Colour[i])
        histogram[-1].SetMarkerSize(1)
        histogram[-1].GetXaxis().SetTitle(Variable)
        histogram[-1].GetYaxis().SetTitle('Entries')

    return

# f_xAOD = r.TFile("/afs/cern.ch/user/s/snoaccor/private/TestArea/xAOD_InputCNN/Merged_user.fernando.data18_13TeV.00358615.physics_Main.merge.AOD.f961_m2020_MYSTREAM_*")
# f_EGAM3 =r.TFile("/afs/cern.ch/user/s/snoaccor/private/TestArea/EGAM3_InputCNN/Merged.user.fernando.data18_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_EGAM3.grp18_v01_p3948_3_MYSTREAM_*")

# --Create TChain
data_xAOD = r.TChain('myTree_Noacco')
data_xAOD.Add("xAOD_InputCNN/Merged_user.fernando.data18_13TeV.00358615.physics_Main.merge.AOD.f961_m2020_MYSTREAM_*")
data_EGAM3 = r.TChain('myTree_Noacco')
data_EGAM3.Add("EGAM3_InputCNN/Merged.user.fernando.data18_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_EGAM3.grp18_v01_p3948_3_MYSTREAM_*")

# def config_Parse():
    # """
    # Set all the configuration to your parser object.

    # # Args::None

    # # Returns::parser object.
    # """
    # parser = argparse.ArgumentParser('name')
    # parser.add_argument('-I', '--Input', required=True, help='<Input folder or file/s>' )
    # parser.add_argument('-O', '--Output', required=True, help='<Output folder or files/s>')
    # parser.add_argument('-D','--Debug', required=False, help='Debug flag', action='store_true')
    # parser.add_argument('-M','--MaxEvents', required=False, help='Set maximum of events. Default -1 == all', type=int, default=-1)
    # return parser

# def main(argv):
    # parser = config_Parse()
    # args = parser.parse_args()

    # input_dir = args.Input()


    # return
# if __name__ == '__main__':
    # main(sys.argv[1:])

#f_xAOD = r.TFile("xAOD_Out/Merged_user.fernando.data18_13TeV.00358615.physics_Main.merge.AOD.f961_m2020_MYSTREAM.root")
#data_xAOD=f_xAOD.myTree_Noacco
for branch in data_xAOD.GetListOfBranches():
    bran_name=str(branch.GetName())

#f_EGAM3 =r.TFile("EGAM_Out/Merged.user.fernando.data18_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_EGAM3.grp18_v01_p3948_2_MYSTREAM_7.root")
#data_EGAM3 = f_EGAM3.myTree_Noacco
for branch in data_EGAM3.GetListOfBranches():
    bran_name=str(branch.GetName())


hists_xAOD=[]

plot_variable='Ph_SS_Reta'
barrelCut ='abs(Phs_eta)<1.37 &&'
plotCut = 'Ph_SS_Reta>0.3 && Ph_SS_Reta<1.02 && Phs_pt>25 &&'
# --Set the cuts
#FixedPZrad = 'abs(Phs_eta)<1.37 && Z_m>80000 && Z_m<100000 && ee_m<83000 && ee_m>45000'
#FixedNotPZrad = '!('+FixedPZrad+')'

#SignalCut_EGAM3='Ph_SS_Reta>0.3 && Ph_SS_Reta<1.02 && Phs_pt>25 && abs(Phs_eta)<1.37  && PZrad'
#SignalCut_EGAM3='Ph_SS_Reta>0.3 && Ph_SS_Reta<1.02 && Phs_pt>25 &&'+FixedPZrad
SignalCut_EGAM3 = plotCut + barrelCut + 'PZrad'

PlotsCuts_xAOD=[]
#PlotsCuts_xAOD.append('Ph_SS_Reta>0.3 && Ph_SS_Reta<1.02 && Phs_pt>25 && abs(Phs_eta)<1.37  && !PZrad')
#PlotsCuts_xAOD.append('Ph_SS_Reta>0.3 && Ph_SS_Reta<1.02 && Phs_pt>25 && abs(Phs_eta)<1.37  && !PZrad && Ph_PtCone20>0.05')
#PlotsCuts_xAOD.append('Ph_SS_Reta>0.3 && Ph_SS_Reta<1.02 && Phs_pt>25 && abs(Phs_eta)<1.37  && !PZrad && Ph_TopoEtCone20>0.05')

#PlotsCuts_xAOD.append('abs(Phs_eta)<1.37 && Ph_SS_Reta>0.3 && Ph_SS_Reta<1.02 && Phs_pt>25'+'&& Ph_TopoEtCone20>0.07')
PlotsCuts_xAOD.append(plotCut + barrelCut + '!PZrad && Ph_TopoEtCone20>0.07')

# legend_xAOD = r.TLegend( 0.2, 0.5, 0.2, 0.5)
# legend_xAOD.SetFillStyle(3000)
# legend_xAOD.SetFillColor(0)
# legend_xAOD.SetBorderSize(0)
# legend_xAOD.SetTextFont(42)
# legend_xAOD.SetTextSize(0.025)

def set_TLegend_style(**kwargs=None):
    """
    Description

    # Args:: optional dict with parameter

    # Returns:: ROOT TLegend instance
    """
    legend = r.TLegend( 0.2, 0.5, 0.2, 0.5)
    legend.SetFillStyle(3000)
    legend.SetFillColor(0)
    legend.SetBorderSize(0)
    legend.SetTextFont(42)
    legend.SetTextSize(0.025)
    return legend

legend_xAOD=set_TLegend_style()
# --Labels
SignalLabel_EGAM3='EGAM3_Signal'

PlotsLabels_xAOD=[]
#PlotsLabels_xAOD.append('xAOD_Bkg true')
#PlotsLabels_xAOD.append('xAOD_Bkg p_T:cone20 Reverted')
PlotsLabels_xAOD.append('xAOD_Bkg e_T:cone20 Reverted')


# --create histo for M_eeg
legend_Meeg=set_TLegend_style()
# legend_Meeg = r.TLegend( 0.2, 0.5, 0.2, 0.5)
# legend_Meeg.SetFillStyle(3000)
# legend_Meeg.SetFillColor(0)
# legend_Meeg.SetBorderSize(0)
# legend_Meeg.SetTextFont(42)
# legend_Meeg.SetTextSize(0.025)

hist_Meeg = r.TH1F('M_eeg','M_eeg',100,45,125)
hist_Meeg.GetXaxis().SetTitle('M_{e^{+}e^{-}#gamma}')
hist_Meeg.GetYaxis().SetTitle('Entries')
data_EGAM3.Project('M_eeg', 'Z_m/1000.', 'abs(Phs_eta)<1.37 && ee_m<83000 && ee_m>45000')
c1=r.TCanvas('Canvas 1')
hist_Meeg.Draw()
legend_Meeg.AddEntry(hist_Meeg,'M_eeg','p')
legend_Meeg.Draw()
c1.Draw()
c1.SaveAs('M_eeg.eps')

# --create the histogram for the signal
hists_EGAM=r.TH1F(SignalLabel_EGAM3,SignalLabel_EGAM3,m_bins,m_min,m_max)
# --Now project data on that hist
print('Projecting...')
data_EGAM3.Project(SignalLabel_EGAM3, plot_variable, SignalCut_EGAM3)

# --Now normalize hist to unity
print('Hist called "'+SignalLabel_EGAM3+'" has '+ str(hists_EGAM.GetEntries())+' entries')
hists_EGAM.Scale(1./hists_EGAM.Integral())
hists_EGAM.SetMarkerColor(Colour[3])
hists_EGAM.SetLineColor(Colour[3])
hists_EGAM.SetMarkerSize(1)
hists_EGAM.GetXaxis().SetTitle(plot_variable)
hists_EGAM.GetYaxis().SetTitle('Entries')

# --Plot all together
Magic(data_xAOD,plot_variable,hists_xAOD,PlotsLabels_xAOD,PlotsCuts_xAOD)

# signal hist is too high!
hists_EGAM.Scale(0.35)
#hists_EGAM.Scale(0.6)


# --Draw
c2 = r.TCanvas('c2')

hists_EGAM.Draw(DrawOptions[0])
legend_xAOD.AddEntry(hists_EGAM,SignalLabel_EGAM3,'p')

for i in range(len(PlotsLabels_xAOD)):
    hists_xAOD[i].Draw(DrawOptions[i+1])
    legend_xAOD.AddEntry(hists_xAOD[i],PlotsLabels_xAOD[i],'p')

legend_xAOD.Draw()

c2.Draw()
c2.SaveAs('bkgStudy.eps')
