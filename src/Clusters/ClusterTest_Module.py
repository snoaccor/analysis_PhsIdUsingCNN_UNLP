"""
This is Python3
"""
# MODULE TO BUILD CLUSTER IMAGES FOR THE
# TESTING OF THE CNN, AND COMPUTING OF
# THE EFFITIENCIES.

import numpy as np
import ClusterTrain_Module
from ROOT import TH2F, TCanvas
#from ClusterTrain_Module import Cluster


def Store_CellData(
        Clusters_Colection, Nth_cluster,Clus_E, Clus_eta, Clus_phi, Cluster_name, Cluster_nbr, Cluster_label=None
        ):
    # -Instantiate the object cluster
    cluster = ClusterTrain_Module.Cluster(
            clusterNbr=Cluster_nbr,
            name=Cluster_name,
            label=Cluster_label,
            clusterArr=None,
            )
    Cluster_nbr+=1

    # --loop through cells
    for cell in Nth_cluster:
        Clus_eta.append(cell[0])
        Clus_phi.append(cell[1])
        Clus_E.append(cell[2])

    # --save data in Cluster atribute: clusterArr. Note that when
    #   instantiated clusterArr = [].
    cluster.clusterArr.append(np.asarray(Clus_eta))
    cluster.clusterArr.append(np.asarray(Clus_phi))
    cluster.clusterArr.append(np.asarray(Clus_E))


    # --if we want to store the whole object then
    Clusters_Colection[cluster.name] = cluster

    # --clear arrays for the new cluster
    Clus_E*=0
    Clus_eta*=0
    Clus_phi*=0

    return

def Get_ClusterDataArray(
        clus_colection, branch, file_name, num_ev, PT,
        PTCone20=None, ETCone20=None, PZR=None, info=False
        ):
    """
    Functionality
    =============
    Given an empty python-dictionary and a TBranch this method will:
        1. check that the TBranch='Cells', because that is where the
        information relevant to this method is stored.

        2. instantiate a member of the class Cluster, storing information.

        3. loop throw the branch and store in the Cluster instance the
        info regarding the ClusterArr atribute.

        4. it will return the input dictionary filled with Cluster Instances.

    Details
    =======
    """

    Clus_E  =[]
    Clus_eta=[]
    Clus_phi=[]
    num_ph = len(PT)
    if info == True: print("Event %s has %s ph" % (num_ev,num_ph))
    num_clus=0

    for clus in branch:    #where branch=myTree.Cells the branch
        clus_len=len(clus)   #size of the cluster

        if info == True:
            print("%s cells in cluster" % clus_len)

        clus_newKey="%s_ev%s_%sPhs_ClusNbr%s"%(file_name,num_ev,num_ph,num_clus)

        Store_CellData(clus_colection, clus, Clus_E, Clus_eta, Clus_phi, clus_newKey, num_clus)

    return clus_colection

def Get_Images(TTree, Filetype, numEv, DEBUG = False):
    Cluster = {}
    pT      = TTree.Phs_pt
    Image_h = 9
    Image_w = 27
    #TODO: need to set everything to go 1 ev at a time instead of
    #   saving in a container.
    # --Build Image of Cluster from the event.
    Cluster = Get_ClusterDataArray(
        clus_colection=Cluster,
        branch=TTree.Cells,
        file_name=Filetype,
        num_ev=numEv,
        PT=pT
        )

    # --Get raw data only.
    Clusters_Data={k: Cluster[k].clusterArr for k in Cluster.keys()}

    # --Map the Cluster Data to matrices and compute the errors
    CMd,ErrClusters=ClusterTrain_Module.MakeOCMd(
        Clusters_Data,
        Image_h,
        Image_w
         )

    # --Compute how many errors where made while mapping Clusters
    #   and delete all Clusters with errors.
    # TODO: fix ErrClusters to not need Labels_data. Also
    # keep in mind that the event could result in being EMPTY
    try:
        for k in ErrClusters.keys():
            del(Cluster[k])
            del(CMd[k])
    except KeyError:
        pass

    skipcond = False
    if len(CMd) == 0:
        skipcond = True

    if DEBUG ==True:
        print(f'{len(ErrClusters)} errors while mapping were made.')
        print(f'{len(CMd)} Clusters.')

    Images =  np.asarray([mat for mat in CMd.values()])
    matrixShape = Images.shape

    Images = Images.reshape(matrixShape + (1,))
    #Images = np.asarray(CMd).reshape(matrixShape + (1,))

    return Images, skipcond

def PassSelectionCNN(ModelPrediction,Eff_cut,DEBUG=False):
    '''
    Given the efficiency cutoff *Eff_cut* decide whether the predicted label is Signal or Background.

    '''

    # models where trained where 1 was Signal
    # 0 was Background, so...
    uncertainty = 0.
    # --Round float to 1 or 0.
    #   setting default to Background.
    predicted_label = 0.
    if ModelPrediction > Eff_cut:
        predicted_label = 1.

    if DEBUG: print(predicted_label)
    # --Compute the uncertainty as the abs difference
    #   between the predicted value and the rounded value.
    uncertainty = abs(predicted_label - ModelPrediction )

    return (predicted_label, uncertainty)
