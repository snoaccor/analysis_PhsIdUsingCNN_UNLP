# -*- coding: utf-8 -*-
"""
This is Python3
"""

import matplotlib.pyplot as plt
import numpy as np
from ROOT import TH2F, TCanvas
from SwanImports import SetAtlasStyle, ATLASLabel
from os.path import join
from os import mkdir
#===================================#
#             Branches
#===================================#
# -- Define some useful dictionaries
def BranchDict(m_TTree,ShowerShapes=False):
    #the branches are atributes to m_TTree.
    branch={
        'Phs_pt':m_TTree.Phs_pt, 
        'Phs_phi':m_TTree.Phs_phi,
        'Phs_eta':m_TTree.Phs_eta,

        'Phs_Loose':m_TTree.Phs_Loose,
        'Phs_Tight':m_TTree.Phs_Tight,
         # --for the record, the 2 following are divided by ph.pt()!!
        'Ph_PtCone20':m_TTree.Ph_PtCone20,
        'Ph_TopoEtCone20':m_TTree.Ph_TopoEtCone20,
        
        'Ph_mc_pdgId':m_TTree.Ph_mc_pdgId,
        'Ph_mc_origin':m_TTree.Ph_mc_origin,
        'Ph_mc_type':m_TTree.Ph_mc_type,
        
        'Ph_isEM':m_TTree.Ph_isEM,
        'Els_pt':m_TTree.Els_pt,
        'Els_phi':m_TTree.Els_phi,
        'Els_eta':m_TTree.Els_eta,
        'Els_m':m_TTree.Els_m,
        'Z_m':m_TTree.Z_m,
        'ee_m':m_TTree.ee_m,
        'Phs_PZrad':m_TTree.PZrad,

        'Cells':m_TTree.Cells,
        }

    # ShowerShapes
    if ShowerShapes == True:
        branch.update({
           'Ph_SS_f1':m_TTree.Ph_SS_f1,
           'Ph_SS_f3':m_TTree.Ph_SS_f3,
           'Ph_SS_Reta':m_TTree.Ph_SS_Reta,
           'Ph_SS_Rhad':m_TTree.Ph_SS_Rhad,
           'Ph_SS_Rphi':m_TTree.Ph_SS_Rphi,
           'Ph_SS_e233':m_TTree.Ph_SS_e233,
           'Ph_SS_e235':m_TTree.Ph_SS_e235,
           'Ph_SS_e237':m_TTree.Ph_SS_e237,
           'Ph_SS_e255':m_TTree.Ph_SS_e255,
           'Ph_SS_e277':m_TTree.Ph_SS_e277,
           'Ph_SS_e2ts1':m_TTree.Ph_SS_e2ts1,
           'Ph_SS_Rhad1':m_TTree.Ph_SS_Rhad1,
           'Ph_SS_ecore':m_TTree.Ph_SS_ecore,
           'Ph_SS_ehad1':m_TTree.Ph_SS_ehad1,
           'Ph_SS_weta1':m_TTree.Ph_SS_weta1,
           'Ph_SS_weta2':m_TTree.Ph_SS_weta2,
           'Ph_SS_ethad':m_TTree.Ph_SS_ethad,
           'Ph_SS_emaxs1':m_TTree.Ph_SS_emaxs1,
           'Ph_SS_emins1':m_TTree.Ph_SS_emins1,
           'Ph_SS_ethad1':m_TTree.Ph_SS_ethad1,
           'Ph_SS_f1core':m_TTree.Ph_SS_f1core,
           'Ph_SS_f3core':m_TTree.Ph_SS_f3core,
           'Ph_SS_fracs1':m_TTree.Ph_SS_fracs1,
           'Ph_SS_DeltaE':m_TTree.Ph_SS_DeltaE,
           'Ph_SS_Eratio':m_TTree.Ph_SS_Eratio,
           'Ph_SS_wtots1':m_TTree.Ph_SS_wtots1,
           'Ph_SS_e2tsts1':m_TTree.Ph_SS_e2tsts1,
           'Ph_SS_widths1':m_TTree.Ph_SS_widths1,
           'Ph_SS_widths2':m_TTree.Ph_SS_widths2,
           'Ph_SS_r33over37allcalo':m_TTree.Ph_SS_r33over37allcalo
           })
    return branch

#===================================#
#           Histograms
#===================================#
# -- Dict for parameters of the histograms
def Set_HistoParam(ShowerShapes=False):
    minh,maxh,binh = {},{},{}

    minh={
        'Phs_pt':-25,'Phs_phi':-5,'Phs_eta':-5,
        'Phs_Loose':-0.5,'Phs_Tight':-0.5, 'Ph_PtCone20':-1,'Ph_TopoEtCone20':-1,
        'Ph_mc_pdgId':-1000,'Ph_mc_origin':-1000,'Ph_mc_type':-1000,
        'Ph_isEM':-20,
        'Els_pt':-25,'Els_phi':-5,'Els_eta':-5,'Els_m':0,
        'Z_m':-25,'ee_m':-25,'Phs_PZrad':-0.1,
         }

    maxh={
        'Phs_pt':400,'Phs_phi':5,'Phs_eta':5,
        'Phs_Loose':1.5,'Phs_Tight':1.5,'Ph_PtCone20':10,'Ph_TopoEtCone20':10,
        'Ph_mc_pdgId':1000,'Ph_mc_origin':1000,'Ph_mc_type':1000,
        'Ph_isEM':30000,
        'Els_pt':400,'Els_phi':5,'Els_eta':5,'Els_m':0.8,
        'Z_m':300,'ee_m':100,'Phs_PZrad':1.1,
        }

    binh={
        'Phs_pt':75,'Phs_phi':50,'Phs_eta':50,
        'Phs_Loose':10,'Phs_Tight':10,'Ph_PtCone20':75,'Ph_TopoEtCone20':75, 
        'Ph_mc_pdgId':50,'Ph_mc_origin':50,'Ph_mc_type':50, 
        'Ph_isEM':50,
        'Els_pt':75,'Els_phi':50,'Els_eta':50,'Els_m':75,
        'Z_m':75,'ee_m':75,'Phs_PZrad':10,
        }
    
    # Shower Shapes
    if ShowerShapes==True:
        minh.update({
            'Ph_SS_f1':-2,
            'Ph_SS_f3':-10,
            'Ph_SS_Reta':-2,
            'Ph_SS_Rhad':-10,
            'Ph_SS_Rphi':-100,
            'Ph_SS_e233':-2000,
            'Ph_SS_e235':-2000,
            'Ph_SS_e237':-2000,
            'Ph_SS_e255':-2000,
            'Ph_SS_e277':-2000,
            'Ph_SS_e2ts1':-500,
            'Ph_SS_Rhad1':-100,
            'Ph_SS_ecore':-2,
            'Ph_SS_ehad1':-5000,
            'Ph_SS_weta1':-2,
            'Ph_SS_weta2':-2,
            'Ph_SS_ethad':-10000,
            'Ph_SS_emaxs1':-500,
            'Ph_SS_emins1':-200,
            'Ph_SS_ethad1':-5000,
            'Ph_SS_f1core':-2,
            'Ph_SS_f3core':-2,
            'Ph_SS_fracs1':-1000,
            'Ph_SS_DeltaE':-10,
            'Ph_SS_Eratio':-2,
            'Ph_SS_wtots1':-1000,
            'Ph_SS_e2tsts1':-100,
            'Ph_SS_widths1':-100,
            'Ph_SS_widths2':-100,
            'Ph_SS_r33over37allcalo':-100,
            })

        maxh.update({
            'Ph_SS_f1':2,
            'Ph_SS_f3':10,
            'Ph_SS_Reta':2,
            'Ph_SS_Rhad':10,
            'Ph_SS_Rphi':100,
            'Ph_SS_e233':10000,
            'Ph_SS_e235':10000,
            'Ph_SS_e237':10000,
            'Ph_SS_e255':30000,
            'Ph_SS_e277':1000,
            'Ph_SS_e2ts1':10000,
            'Ph_SS_Rhad1':100,
            'Ph_SS_ecore':2,
            'Ph_SS_ehad1':5000,
            'Ph_SS_weta1':2,
            'Ph_SS_weta2':2,
            'Ph_SS_ethad':10000,
            'Ph_SS_emaxs1':50000,
            'Ph_SS_emins1':5000,
            'Ph_SS_ethad1':10000,
            'Ph_SS_f1core':2,
            'Ph_SS_f3core':2,
            'Ph_SS_fracs1':1000,
            'Ph_SS_DeltaE':5000,
            'Ph_SS_Eratio':2,
            'Ph_SS_wtots1':1000,
            'Ph_SS_e2tsts1':10000,
            'Ph_SS_widths1':100,
            'Ph_SS_widths2':100,
            'Ph_SS_r33over37allcalo':100,
            })

        binh.update({
           'Ph_SS_f1':50,
           'Ph_SS_f3':50,
           'Ph_SS_Reta':50,
           'Ph_SS_Rhad':50,
           'Ph_SS_Rphi':50,
           'Ph_SS_e233':50,
           'Ph_SS_e235':50,
           'Ph_SS_e237':50,
           'Ph_SS_e255':50,
           'Ph_SS_e277':50,
           'Ph_SS_e2ts1':50,
           'Ph_SS_Rhad1':50,
           'Ph_SS_ecore':50,
           'Ph_SS_ehad1':50,
           'Ph_SS_weta1':50,
           'Ph_SS_weta2':50,
           'Ph_SS_ethad':50,
           'Ph_SS_emaxs1':50,
           'Ph_SS_emins1':50,
           'Ph_SS_ethad1':50,
           'Ph_SS_f1core':50,
           'Ph_SS_f3core':50,
           'Ph_SS_fracs1':50,
           'Ph_SS_DeltaE':50,
           'Ph_SS_Eratio':50,
           'Ph_SS_wtots1':50,
           'Ph_SS_e2tsts1':50,
           'Ph_SS_widths1':50,
           'Ph_SS_widths2':50,
           'Ph_SS_r33over37allcalo':50,
            })
    
    return minh,maxh,binh

def Fill_Histo(histograms,bkey,value):
    if bkey in ['Phs_pt','Els_pt','ee_m']:
    #if bkey in ['Phs_pt','Els_pt','Z_m','ee_m']:
        h_val=value/1000. #fill in GeV
        histograms[bkey].Fill(h_val)
   
    # --Since Z_m branch holds actually M_eeg data,
    #   we need to add the PZrad criteria to get Z_m
    Zmval=False
    if bkey == 'Phs_PZrad':
        histograms[bkey].Fill(value)
        if value > 0:
            Zmval = True
    if (bkey == 'Z_m' and Zmval):
        histograms[bkey].Fill(value/1000.)

    #elif bkey in ['Ph_PtCone20','Ph_TopoEtCone20']:
    #    h_val=j/1000. #fill in GeV

    #    assert TotNbrEntries[bkey]!=0, 'Zero entries at %s'%bkey
    #    h_val/=TotNbrEntries[bkey]   #normalize by nbr entries
    #    histograms[bkey].Fill(h_val)            

    elif bkey == 'Cells':
        pass     # will treat this separately
    else: 
        histograms[bkey].Fill(value)
    return

def Plot_Histo(BranchDict,Histograms,Save=True,File_format='.png',Show=False):
    SetAtlasStyle()
    Canvas = {}

    try:
        mkdir('Output_Histograms')
    except: pass

    for bkey in BranchDict.keys():
        if bkey == 'Cells': continue
        Canvas[bkey]=TCanvas("c"+bkey)
        Histograms[bkey].Draw()
        Canvas[bkey].Draw()
        ATLASLabel(0.55, 0.85, "Work in Progress",1)
        Outputfile_name = join('Output_Histograms',bkey + File_format)
    
        if Save: Canvas[bkey].SaveAs(Outputfile_name) 
    
    return #Canvas

#===================================#
#         Cluster Handling
#===================================#

"""
=== Notes ===
*must pass an empty pyhton dictionary as clus_dict

*branch must be the one containing the cells.

*we are assuming an order in the branch namely: 
    1st element of cell is the 'eta' value,
    2nd element of the cell is the 'phi' value,
    3rd element of the cell is the 'energy' value.
=============

"""
def Ans_HowManyClusters(label_d,DEGUG=False):
    cnt_bkg=0
    cnt_sig=0
    for kk in label_d.keys():
        if label_d[kk] == 'bkg':
            cnt_bkg+=1
        if label_d[kk] == 'signal':
            cnt_sig+=1
    if DEGUG==True:
        print('total nbr of clusters: %s' %(cnt_bkg+cnt_sig))
        print('we have %s background clusters & %s signal clusters' %(cnt_bkg,cnt_sig))
    
    return cnt_sig,cnt_bkg

class Cluster(object):
    """
    Store all information related to the clusters built from cells
    
    Details:
    ========
        *clusterNbr: number of cluster.
        
        *name: name of the cluster.
        
        *pZR: bool, defaul None, if False this means that the Egamma object
        related to this cluster didn't pass the ZRAD criteria.
        
        *clusterArr: array of shape 3 of the cluster data, meaning
        clusterArr = [eta[eta1,...,etaN],phi[...],E[...]].
    """
    
    def __init__(self,clusterNbr,label=None,name=None,clusterArr=None,Ctype="EGammaPhoton"):    
       
        self.clusterNbr=clusterNbr
        
        # label:: whether it's a photon from the unbiased-background
        #         or it's signal
        if label != None:
            self.label = label
        
        assert(type(name)==str),"name is a string"
        if name != None:
            self.name = name
        
        # ClusterArr: array of the cluster data, meaning
        # ClusterArr=[eta[eta1,...,etaN],phi[...],E[...]]
        if clusterArr==None:
            self.clusterArr = []
        else:
            self.clusterArr = clusterArr

        return
    
    @property
    def eta(self):
        return self.clusterArr[0]
    @property
    def phi(self):
        return self.clusterArr[1]
    @property
    def E(self):
        return self.clusterArr[2]
    @property
    def _size_(self):
        size=len(self.clusterArr[2])
        return size
    @property
    def deltaEta(self):
        Deta=[self.eta[0]-self.eta[x] for x in range(len(self.eta))]
        return Deta
    @property
    def deltaPhi(self):
        Dphi=[self.eta[0]-self.phi[x] for x in range(len(self.phi))]
        return Dphi

#def Set_label(pT,pZR,reverted_iso):
def Set_label(pT,pZR,m_eeg,reverted_iso):
    label = "not_good_bkg"
    #if (pZR==1.0):
    if (pZR==1.0 and 80000< m_egg <100000):
        label = "signal"
    elif (reverted_iso>0.0):
        #  note that this implies PZR == False, 
        #  which is good cause we don't want false background positives.
        label = "bkg"    #short for 'background'
    return label

def Store_CellData(
        Clusters_Colection, Nth_cluster,Clus_E, Clus_eta, Clus_phi, Cluster_name, Cluster_nbr, Cluster_label=None
        ):
    # -Instantiate the object cluster
    cluster = Cluster(
            clusterNbr=Cluster_nbr,
            name=Cluster_name,
            label=Cluster_label,
            clusterArr=None,
            )
    Cluster_nbr+=1

    # --loop through cells
    for cell in Nth_cluster:
        Clus_eta.append(cell[0])
        Clus_phi.append(cell[1])
        Clus_E.append(cell[2])

    # --save data in Cluster atribute: clusterArr. Note that when
    #   instantiated clusterArr = [].
    cluster.clusterArr.append(np.asarray(Clus_eta))
    cluster.clusterArr.append(np.asarray(Clus_phi))
    cluster.clusterArr.append(np.asarray(Clus_E))


    # --if we want to store the whole object then
    Clusters_Colection[cluster.name] = cluster

    # --clear arrays for the new cluster
    Clus_E*=0
    Clus_eta*=0
    Clus_phi*=0

    return

def Get_ClusterDataArray(
        clus_colection,branch,file_name,num_ev,
        PT,ETA,
        PZR,ReverseIso,Meeg,
        info=False
        ):
    """
    Functionality
    =============
    Given an empty python-dictionary and a TBranch this method will:
        1. check that the TBranch='Cells', because that is where the 
        information relevant to this method is stored.
        
        2. instantiate a member of the class Cluster, storing information.
    
        3. loop throw the branch and store in the Cluster instance the
        info regarding the ClusterArr atribute.
        
        4. it will return the input dictionary filled with Cluster Instances.
        
    """
    
    Clus_E=[]
    Clus_eta=[]
    Clus_phi=[]
    num_ph = len(PT)
    if info == True: print("Event %s has %s ph" % (num_ev,num_ph))
    num_clus=0
    
    #for clus,v_pT,v_eta,v_pZR,v_eTcone20 in zip(branch,PT,ETA, PZR, ReverseIso):    #where PZR=myTree.PZR the branch
    for clus,v_pT,v_eta,v_pZR,v_eTcone20,v_Meeg in zip(branch,PT,ETA, PZR, ReverseIso, Meeg):    #where PZR=myTree.PZR the branch
        
        clus_len=len(clus)   #size of the cluster
        
        # --Train only Barrel
        if abs(v_eta) > 1.37: continue
        
        # --Train only pT>20 GeV
        if v_pT < 20000: continue
        
        if info == True: 
            print("%s cells in cluster" % clus_len)
            print("PZR:%s" %v_pZR,"cluster Iter:%s"%num_clus)

        clus_newKey="%s_ev%s_%sPhs_ClusNbr%s"%(file_name,num_ev,num_ph,num_clus)
        
        # --compute the reverse isolation (shifting to 0); 
        #   for reference for the background we need ph_ptcone20>0.05*ph_pt & ph_etcone20>0.07*ph_pt
        #   but REMEMBER from CellDumpingPackage the values stored in the Branches ph_ptcone20 & ph_etcone20 are
        #   already divided py ph_pT (cause life it's hard bro)... Therefore:
        
        # --Lets use reverted_iso = EtCone20
        v_eTcone20-=0.07
        #clabel = Set_label(v_pT,v_pZR,v_eTcone20)
        clabel = Set_label(v_pT,v_pZR,v_Meeg,v_eTcone20)
        
        # --Only save relevant Clusters meaning: signal or unbiased-bkg
        if clabel != "not_good_bkg":
            Store_CellData(
                    clus_colection, clus,
                    Clus_E, Clus_eta, Clus_phi,
                    clus_newKey, num_clus,
                    clabel) 
        
    return clus_colection

def GetStepsToOrigin(Cluster):
    """
    To map the rest of the cells to matrix we need to map them according to eta_0=0 & phi_0=0
    We need to define minumum distance between cells, which we will call stepX; we will have one stepEta 
    and one stepPhi
    """
    
    #This values have been taken from bibliography
    stepEta=0.025
    stepPhi=0.02449

    # --Because phi is the azimutal angle that lies in [-pi,pi] and there is a cut at the borders,
    # some cells get truncated. To fix this use the symmetry by substracting a pi phase
    # to those cells with a phi coord distance to de central cell greater than Pi
    
    #DistToPhi=Cluster[1] - phi_0 which is obviously just Cluster[1]. so:
    DistToPhi = Cluster[1]
    i=0
    for distphi in DistToPhi:
        if abs(distphi) >= np.pi:
            # fix the value; in this case always a positive shift.
            Cluster[1][i]+=np.pi
            i+=1
        else: i+=1

    # --asuming the origin of eta is the center of the cell but shifted.
    eta_shift= -0.0265
    STP_eta = np.round((Cluster[0]+(stepEta/2)*np.sign(Cluster[0]))/stepEta + eta_shift)
    # --asuming the origin of eta is at the vertix of the cell.
#         STP_eta = np.round(Cluster[0]/stepEta)
    # --asuming the origin of eta is the vertix of the cell, but shifted.
#         STP_eta = np.round(Cluster[0]/stepEta + eta_shift)
    
    # --asuming the origin of phi is at the center of the cell.
    STP_phi = np.round((Cluster[1]+(stepPhi/2)*np.sign(Cluster[1]))/stepPhi)
    # --asuming the origin of phi is the vertix of the cell.
#         STP_phi = np.round(Cluster[1]/stepPhi)
    
    # --since we need integer steps, and we are using np.array objects:
    #StepsToOriginDict[k]=[
    StepsToOrigin=[
            STP_eta.astype(int),
            STP_phi.astype(int),
            Cluster[2]
            ]    
        
    return StepsToOrigin

def GetCCMd(StepsToCenter,dimX,dimY,ErrInfo=None):
    """
    Aim:
    ===
        Given a dictionary holding the integer steps mapping of the cells to a certain origin,
        this function returns a dimX*dimY matrix of the Energy distribution of the cells. 
        basically giving the cluster image.

        CCMd: CenteredClusterMatrixdictionary
    """

    #assert (dimX%2==1 and dimY%2==1), "dims must be odd"
    if ErrInfo == None: ErrInfo=False
        
    # the center of the matrix is at (dim-1)/2 so:
    cX_Coord = (dimX-1)//2
    cY_Coord = (dimY-1)//2
    
    STC=StepsToCenter
    
    # --Create a dimX x dimY matrix, must have odd dimensions.
    CCM=np.zeros((dimX,dimY))
    
    nb_ErrCell=0
    # -- Map energies to matrix elements.
    for row,col,E in zip(STC[0],STC[1],STC[2]):
        if CCM[cX_Coord+row,cY_Coord+col]!=0:
            
            if ErrInfo == True:
                ErrMessage="in cell(%s,%s),prev E: %s, new E: %s"%(
                            cX_Coord+row, cY_Coord+col,CCM[cX_Coord+row,cY_Coord+col],E
                            )
                print(ErrMessage) 
            
            nb_ErrCell+=1                
            CCM[cX_Coord+row,cY_Coord+col]+=E
            
#             if abs(row) >cX_Coord/2: print("x:%s at %s"%(cX_Coord+row,k))
#             if abs(col) >cY_Coord/2: print("y:%s at %s"%(cY_Coord+col,k))
                
        else:
            CCM[cX_Coord+row,cY_Coord+col]=E
        
    return CCM,nb_ErrCell

def NormalizeE(ClusterMatrix):
    try:
        maxEvalue = ClusterMatrix.max()
    # --Since sometimes the minimum is not 0 lets fix that
        minEvalue = ClusterMatrix.min()
        for row in range(len(ClusterMatrix)):
             for col in range(len(ClusterMatrix[0])):
                 ClusterMatrix[row,col] = (ClusterMatrix[row,col]+abs(minEvalue))/(maxEvalue+abs(minEvalue))

    except ValueError:
        print("corrupted cluster")
    
    return ClusterMatrix

def WindowRisizing(Matrix,Matrix_Xdim,Matrix_Ydim):
    # --Get the indeces of the Max Value of Energy: (n_max,m_max)
    n_max, m_max = np.unravel_index(Matrix.argmax(), Matrix.shape)

    # --Set the boundaries of the new Matrix
    #p = round(Matrix_Xdim/2) + 1
    #q = round(Matrix_Ydim/2) + 1
    
    #p = round(Matrix_Xdim/2)
    #q = round(Matrix_Ydim/2)
    p = int(Matrix_Xdim/2)
    q = int(Matrix_Ydim/2)
    # --Define the New Matrix
    # --Define the New Matrix
    # here we are taking a subset of the original matrix arround the highest E-value.
    # According to the dessired matrix shape
    New_Matrix = Matrix[int(n_max-p) : int(n_max+p) ,int(m_max-q) : int(m_max+q)]
    
    # this assert is not working correctly
    assert np.any(New_Matrix.shape!=0), 'dim %s of the Matrix is too big'% np.where(New_Matrix.shape==0)
    
    return New_Matrix #New_OCMd
      
def MakeOCMd(Clusters_Data,Matrix_Xdim,Matrix_Ydim,ErrInfo=None):
    if ErrInfo == None: ErrInfo=False
    """
    Input::
        Clusters_Data: dictionary of clusters filled with [eta,phi,E] arrays.
        Matrix_dim: desired dimensions of the output matrices.
    
    Output::
        Norm_CCMd: dictionary filled with normalized cluster matrices.
    """
    # --To improve memory usage lets loop one cluster at a time:
    Norm_OCMd={}
    ErrClusters={}
    TotMapErr1=0
    for k in Clusters_Data.keys():
        clus= Clusters_Data[k]
        
        StepsToOrigin=GetStepsToOrigin(clus)
    
        # --Create the CenterClusterMatrix
        
        OCM,nb_ErrCell =GetCCMd(StepsToOrigin,205,305,ErrInfo=ErrInfo)
        if nb_ErrCell>0:
            TotMapErr1+=nb_ErrCell
            ErrClusters.update({k:nb_ErrCell})

        # --Resize the Matrix
        OCM = WindowRisizing(OCM,Matrix_Xdim,Matrix_Ydim)
        
        # --Normalize each cluster matrix in the OCMd
        Norm_OCM=NormalizeE(OCM)
        
        # --Save the cluster if it's not empty.
        for col in Norm_OCM:
            if any(not elem for elem in col):
                pass
            else: Norm_OCMd.update({k:Norm_OCM}) 
    return Norm_OCMd, ErrClusters

def Reduce_Amount_Data(Filename,Clusters_Data,Labels_Data,Clusters_Colection):
    """
    Since we are looping through Folders and file types 
    secuentially we are going to load 1 type of data at a time.
    """
    f_aux = Filename.split('_')[0] 
    for k,v in list(Labels_Data.items()):
        notSignal_cond = v == 'bkg' and f_aux == 'EGAM3'
        notBkg_cond = v == 'signal' and f_aux == 'xAOD'
        
        # --If we are reading EGAM3 we don't want to store
        #   any bkg info, so we might as well delete it.
        #   And viceversa with AOD.
        
        if notSignal_cond or notBkg_cond:
            del(Clusters_Data[k])
            del(Labels_Data[k])
            del(Clusters_Colection[k])
        
    return  Clusters_Data, Labels_Data, Clusters_Colection     

def Del_ErrClus(ErrDict,Labels_Data, Clusters_Colection,RealCCMd):
    cnt_s, cnt_b =Ans_HowManyClusters(Labels_Data,True)
    cntErr=0
    for kk in ErrDict.keys():
        try:
            lab = Labels_Data[kk]
            if lab=='signal':       
                cntErr+=1
               
            del(Labels_Data[kk])
            del(Clusters_Colection[kk])
            del(RealCCMd[kk])
        except KeyError:
            pass

    percent=round(cntErr/(cnt_s+cnt_b),2)*100
    print(f'* {percent}% of clusters with errors; deleted*')
        
    return Labels_Data, Clusters_Colection, RealCCMd

def Get_LabeledImages(Signal,Bkg,CMd,Labels_d,add_more_bkg,DEGUG=False):
    # --this reads: for k in CMd.keys() check if the label is =signal,
    #   if so then store de cluster matrix in the list.
    Signal += [(CMd[k],1) for k in CMd.keys() if Labels_d[k]=='signal']
    Bkg += [(CMd[k],0) for k in CMd.keys() if Labels_d[k]=='bkg']

    # --Checking no mistakes where made.
    if DEGUG:
        for tup in Signal:
            assert tup[0].size > 0, 'corrupted signal cluster'
        for tup in Bkg:
            assert tup[0].size > 0, 'corrupted bkg cluster'
        print('label:%s, filename:%s, load_cond: %s' %(Label,Filename,cond))
    # -- Keep as much Background as signal.
    try:
        assert len(Bkg)>=len(Signal), 'not enough bkg.'
        # -- Check if in the last batch we had more signal than bkg
        if add_more_bkg !=0:
            # --Add enough bkg to compensate for the missing bkg.
            Bkg = Bkg[:len(Signal)+add_more_bkg]
            # --Set missing bkg to 0.
            add_more_bkg=0
        else:
            Bkg = Bkg[:len(Signal)]
    except AssertionError:
        # --Set how much bkg is missing
        add_more_bkg += len(Signal) - len(Bkg)

    return Signal, Bkg, add_more_bkg

def Get_FormatedData(Signal, Bkg, Channels, Containers_size):
    from random import sample
    # --Make both Containers the same size.
    #   note that if any of them are smaller than Containers_size
    #   no change will be applied to its size.
    Signal = Signal[:Containers_size]
    Bkg = Bkg[:Containers_size]

    # --Add both lists 
    Data = Signal + Bkg
    
    # --Sort them randomly
    Data = sample(Data,len(Data))     
    
    # --Divide images & labels in 2 arrays.
    Images = np.asarray([tup[0] for tup in Data])
    Labels = np.asarray([tup[1] for tup in Data])

    # --Get the shape of the Images; (number_of_images,dimX,dimY)
    newshape = Images.shape + (Channels,)
    
    # --Reshape the Images list to feed the CNN
    Images=Images.reshape(newshape)
    
    return Images, Labels


#===================================#
#             Plotting 
#===================================#
def PlotClusterImage(clus_dict,centering=False):
    Clusters_Image={}
    Canvas = {}
    
    for clusKey in clus_dict.keys():
        
        #create image using TH2F a 2D histogram
        
        #set boundaries for the image
        x_min=min(clus_dict[clusKey][0])
        x_max=max(clus_dict[clusKey][0])
        y_min=min(clus_dict[clusKey][1])
        y_max=max(clus_dict[clusKey][1])
        
        #to center de axes, just take the average of the extremes
        Cx = (x_max+x_min)/2
        Cy = (y_max+y_min)/2
        
        Dx = x_max-x_min
        Dy = y_max-y_min
        
        xbin = int(Dx/0.025)*2
        ybin = int(Dy/0.025)*2
        
         #using TH2F
        histo_name='clust'+str(clusKey) #set the name for the image of the cluster
        
        if centering == True:
            Clusters_Image[clusKey]=TH2F(histo_name,'Clusters',xbin,x_min-Cx,x_max-Cx,ybin,y_min-Cy,y_max-Cy)
            Clusters_Image[clusKey].GetXaxis().SetTitle('#Delta#eta')
            Clusters_Image[clusKey].GetYaxis().SetTitle('#Delta#phi')
        else:
            Clusters_Image[clusKey]=TH2F(histo_name,'Clusters',xbin,x_min,x_max,ybin,y_min,y_max)
            Clusters_Image[clusKey].GetXaxis().SetTitle('#eta')
            Clusters_Image[clusKey].GetYaxis().SetTitle('#phi')
        
        #Unpack data from the Cluster Dictionary to store in histogram          
        for c_eta,c_phi,c_E in zip(clus_dict[clusKey][0],clus_dict[clusKey][1],clus_dict[clusKey][2]):
            
            #center the cells if needed
            if centering == True:
                cell_x = c_eta - Cx
                cell_y = c_phi - Cy
            else:
                cell_x= c_eta
                cell_y= c_phi
            
            #Fill the histogram
            Clusters_Image[clusKey].Fill(cell_x,cell_y,c_E)


        Canvas[clusKey]=TCanvas("c"+str(clusKey))
        Clusters_Image[clusKey].Draw("COLZ")

#       ATLASLabel(0.55, 0.85, "Work in Progress",1)
        Canvas[clusKey].Draw()

    
    return Clusters_Image

def PlotCM(Cluster,Cluster_name,SHOW=False,SAVE=True):
    fig_clus = plt.figure()
    plt.imshow(Cluster,cmap=plt.cm.binary,aspect='auto')
    plt.colorbar(orientation="horizontal")
    title = '$ATLAS$'+' Work in progress - ' + str(Cluster_name)
    plt.title(title)
    if SHOW: plt.show()
    #if SAVE: plt.savefig('Cluster_'+Cluster_name+'.png')
    if SAVE:
        fig_clus.savefig('Cluster_'+Cluster_name+'.png')
        #fig_clus.close()
    return
