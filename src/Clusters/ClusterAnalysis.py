#!/usr/bin/env python3
# coding: utf-8

"""
We will get, plot and save cells from a TTree file and also some relevant histograms
"""
# --Basic imports

import ROOT as r
import matplotlib.pyplot as plt
import numpy as np
import time
# -- for Raw Data manipulation
import ClusterTrain_Module
# -- for Neural Network
import CNN_Module
from keras.callbacks import CSVLogger
# -- for file loading
from os import listdir
from os.path import isfile, join
# -- for Plots format.
from SwanImports import SetAtlasStyle


SetAtlasStyle()

# --Setting global parameters

# TODO: add parse options
TotNbrEv=0
batch_size = 45000      # size batch to loop through a file.
num_Images=60000        # size container of Signal & Background images
Image_h=9
Image_w=27
Channels = 1            # since we have monochromatic scale
Epochs = 30

# --Shape of each matrix that goes throw the CNN.
Input_shape = (Image_h-1, Image_w-1, Channels)

# TODO goes to TRAINING
# --Choose the CNN model
myModel=[]
# --This is how one instantiates the model
# --Choose your model defining the architecture
myModel.append(CNN_Module.MyModels(
    'model0',
    architecture=CNN_Module.model0(Input_shape)
    ))
myModel.append(CNN_Module.MyModels(
    'model1',
    architecture=CNN_Module.model5(Input_shape)
    ))

# --FileNames for the model
for model in myModel:
    model.Set_Model_dir
    model.TrainLogs_file
    model.Summary_file
    model.Model_file
    model.History_files


# ### Add the files to their container
#
# The *files* must be Output files from the [CellDumpingPackage](https://gitlab.cern.ch/snoaccor/CellDumpingPackage).
# This file will hold a $NTuple$ stored in a $Tree$,
# with all the relevant information.
# Each piece of data will be stored in a $Branch$.


#path_EGAM3="/eos/user/f/fernando/public/user.fernando.data18_13TeV.00356205.DAOD_EGAM3.f956_m2004_p3583_MYSTREAM"
#path_xAOD="/eos/user/f/fernando/public/user.fernando.data18_13TeV.00358615.physics_Main.merge.AOD.f961_m2020_MYSTREAM"

path_EGAM3="EGAM3_InputCNN"
path_xAOD="xAOD_InputCNN"
#path_EGAM3="EGAM_Out"
#path_xAOD="xAOD_Out"

path_to_dirs={'EGAM3':path_EGAM3,'xAOD':path_xAOD}

# --Define some useful functions
def log_cond(Signal_container, Bkg_container, ContainerSize, Filename,DEBUG=False):
    """This function returns bool according to the condition of the
       length of the containers."""
    #dif_len = len(Signal) - len(Bkg)
    tot_len = len(Signal_container) + len(Bkg_container)
    # --Check Max size of container isn't reach
    f_aux = Filename.split('_')[0]
    s_cond = (f_aux == 'EGAM3') and (len(Signal_container) < ContainerSize)
    b_cond = (f_aux == 'xAOD') and (len(Bkg_container) < ContainerSize)

    #cond = (dif_len != 0) and ( tot_len <= 2*ContainerSize)
    cond = (s_cond or b_cond) and ( tot_len < 2*ContainerSize)
    assert type(cond) == bool, 'sth went wrong at log_cond'
    if DEBUG == True:
        print(f's_cond {s_cond}; b_cond {b_cond}; cond {cond}')
    yield cond

def Separate_Data(TFile_name,TTree,BranchDict,Histograms,Clusters_Colection,Event):
    """
    This method takes a TTree, a dictionary with branches, a dictionary of Histograms,
    a dictionary Clusters_Colection and an Event.
    It will store the information of the branches to the histograms, except for the
    branch 'Cells' which will store the Cluster data in the Clusters_Colection.
    """
    # --Loop through branches
    for bkey in BranchDict.keys():
        # --Loop through data stored in branch
        for data in BranchDict[bkey]:
            # --Store data in Histogram
            ClusterTrain_Module.Fill_Histo(Histograms,bkey,data)

        # --Train only Barrel
        if bkey == 'Cells':

            pT = TTree.Phs_pt
            eta = TTree.Phs_eta
            #check how many Phs are in that entry; we don't care for empty events
            if len(pT)==0:continue

            # --For the labeling we need pT, PZrad, PtCone20 & TopoEtCone20 data, so we get for each event. However
            #   since PtCone20 & TopoEtCone20 are already divided by pT form the package...
            pZR = TTree.PZrad
            #pTcone20=TTree.Ph_PtCone20
            topoeTcone20=TTree.Ph_TopoEtCone20
            m_eeg=TTree.Z_m

            # --Store info in Dictionary of Cluster objects
            Clusters_Colection=ClusterTrain_Module.Get_ClusterDataArray(
                Clusters_Colection,
                BranchDict[bkey],
                TFile_name,
                Event,
                PT=pT,
                ETA=eta,
                PZR=pZR,
                ReverseIso=topoeTcone20,
                Meeg=m_eeg,
                info=False
                )
    return Clusters_Colection

def Set_BeginEnd(NbrBatches,BatchSize,MinNbr,BatchIter):
    if NbrBatches==1:
        _begin = 0
        _end = MinNbr
    else:
        _begin = BatchIter*BatchSize
        _end = (BatchIter+1)*BatchSize
    return _begin, _end

def Set_Histo(histo_dict,histo_name,binh,minh,maxh):
    for key in minh.keys():
        histo_dict[key]=r.TH1F(histo_name[key],
                          histo_name[key],
                          binh[key],
                          minh[key],
                          maxh[key])
        histo_dict[key].GetXaxis().SetTitle(str(key))
        histo_dict[key].GetYaxis().SetTitle('Entries')
        ending=key.split('_')[1]
    #     print(ending)
        if ending == 'pt' or ending == 'm':
            histo_dict[key].GetXaxis().SetTitle(str(key)+'[GeV]')
    return

def StoreEventInfo(Begin,End,FileData,Clusters_Colection,Histograms):
    """
    FileData format :: [Filename,TTree,BranchDict]
    """
    for ev in range(Begin,End):
        # --Get the event from the TTree
        FileData[1].GetEntry(ev)
        Clusters_Colection = Separate_Data(
                TFile_name=FileData[0],
                TTree=FileData[1],
                BranchDict=FileData[2],
                Histograms=Histograms,
                Clusters_Colection=Clusters_Colection,
                Event=ev,
                )
    return Clusters_Colection, Histograms

def Data_Loader(
            Files_Data, NbrBatches, BatchSize, MinNbr, BatchIter,
            Histograms, Signal, Background,
            Image_h, Image_w, add_more_bkg
            ):

    # --Set the dictionary to store the Cluster Objects.
    Clusters_Colection={}
    _begin, _end = Set_BeginEnd(NbrBatches,BatchSize,MinNbr,BatchIter)

    # --Loop through events & store raw data
    for filedata in Files_Data:
        Clusters_Colection, histograms = StoreEventInfo(_begin,_end,filedata,Clusters_Colection,Histograms)
    # --Get Raw-Data for the CNN
    # Clusters_Data={'clus_1':[Clus_eta,Clus_phi,Clus_E],...}
    Clusters_Data={k: Clusters_Colection[k].clusterArr for k in Clusters_Colection.keys()}
    Labels_Data={k: Clusters_Colection[k].label for k in Clusters_Colection.keys()}

    filename=Files_Data[0][0]
    # --Get rid of excesive data
    Clusters_Data, Labels_Data, Clusters_Colection = ClusterTrain_Module.Reduce_Amount_Data(
            filename,
            Clusters_Data,
            Labels_Data,
            Clusters_Colection
            )

    # --We also don't want to load more info
    #   than the required for training
    container_max_size = num_Images//2
    Clusters_Data = dict(list(Clusters_Data.items())[:container_max_size])
    Labels_Data = dict(list(Labels_Data.items())[:container_max_size])

    # --Map the Cluster Data to matrices and compute the errors
    RealCCMd,ErrClusters=ClusterTrain_Module.MakeOCMd(
            Clusters_Data,
            Image_h,
            Image_w
            )

    # --Clear Clusters_Data not useful anymore
    #Clusters_Data.clear()       #TODO:check this dosn't cause problems

    # --Compute how many errors where made while mapping Clusters
    #   and delete all Clusters with errors.
    Labels_Data, Clusters_Colection,RealCCMd = ClusterTrain_Module.Del_ErrClus(
            ErrClusters,
            Labels_Data,
            Clusters_Colection,
            RealCCMd
            )

    # --Set appropiate format for the CNN
    Signal,Background,add_more_bkg = ClusterTrain_Module.Get_LabeledImages(
            #filename,
            Signal,
            Background,
            RealCCMd,
            Labels_Data,
            add_more_bkg
            )

    # --Plot some if you wish
    j=0
    while j < 3:
        Cluster_name, Cluster = list(RealCCMd.items())[j]
        ClusterTrain_Module.PlotCM(Cluster, Cluster_name, SAVE=True)
        j+=1

    return

 ### *Histograms*

# --We define all the histograms as follows:
# -- Dict for parameters of the histograms
minh, maxh, binh = ClusterTrain_Module.Set_HistoParam(ShowerShapes=True)
histo_name={key:'h_'+str(key) for key in minh.keys()}
print(histo_name)

histograms={} # We store all histograms here.


Set_Histo(histograms,histo_name,binh,minh,maxh)

# -- Count the number of directories
path_Tot=len(path_to_dirs)
path_cnt = 1

# -- Define empty list to store the data for the CNN
Signal=[]
Background=[]

add_more_bkg=0
cond_to_log_files = True

print('Begining Loading Data ...')
# -- Loop through directories of files.
for dir_name, path in path_to_dirs.items():
    filename = dir_name
    print('Reading directory %s, %s/%s' % (str(path),path_cnt,path_Tot))
    path_cnt+=1
    # --Make a list of all the files in the directory.
    FileList = [f for f in listdir(path) if isfile(join(path, f))]
    totFiles = len(FileList)
    f_cnt=1

    for files in FileList:
        # --Open the file
        m_f=r.TFile(join(path,files))
        m_tree= m_f.Get('myTree_Noacco')
        numEv = m_tree.GetEntries()
        for i in range(numEv):
            m_tree.GetEntry(i)
        branches = ClusterTrain_Module.BranchDict(m_tree, ShowerShapes=True)
        filename += '_'+str(f_cnt)
        Files_Data=[[filename,m_tree,branches]]
        print('  ')
        print('reading %s/%s files' %(f_cnt,totFiles))
        print('file name:%s' % filename)
        f_cnt+=1
        TotNbrEv+=numEv
        batches = numEv//batch_size
        if batch_size >= numEv:
            batches = 1

        for b in range(batches):
            if cond_to_log_files:
                print('batch %s of %s'% (b+1,batches))
                Data_Loader(
                        Files_Data,batches,batch_size,
                        numEv, b, histograms,
                        Signal, Background,
                        Image_h, Image_w, add_more_bkg
                        )
            #else: break     #this way we avoid reading more unnecessary files.

            container_max_size = num_Images//2
            # --Update condition to load data.
            cond_to_log_files = next(log_cond(Signal,Background,container_max_size,filename))
            #cond_to_log_files = next(log_cond(Signal,Background,num_Images))
            #print(cond_to_log_files)

        print(TotNbrEv)
        print('So far we have %s point of data' % (len(Signal)+len(Background)))

print('Finished Loading Data.')
ClusterTrain_Module.Plot_Histo(branches, histograms)
#c = r.TCanvas('Canvas')
#for name, histo in histograms.items():
#    histo.Draw('AZEP')
#    ATLASLabel(0.55, 0.85, "Work in Progress",1)
print(f'Histrograms Saved...')

print(f'Total training data = {len(Signal)+len(Background)}')

Images, Labels = ClusterTrain_Module.Get_FormatedData(Signal,Background,Channels,num_Images)

# -- Save Images and Labels to NPY format
np.save(Images,f'{Output}/cluster_images.npy',allow_pickle=True)
np.save(Labels,f'{Output}/cluster_labels.npy',allow_pickle=True)



# TODO -> goes to TRAINING
# --Train the CNN
x,y = Images, Labels #TrainDataSet_Im,TrainDataSet_Lab
print('Begining To Train the Model')
for models in myModel:
    train_tic = time.perf_counter()
    models.architecture = CNN_Module.train_model(
        x,
        y,
        CNN_model=models.architecture,
        EPOCHS=Epochs,
        VALIDATION_SPLIT=0.2,
        TRAINING_LOGS_FILE=models.TrainLogs_file,
        MODEL_FILE=models.Model_file,
    )

    train_toc = time.perf_counter()
    print("Model trained: %s" % models.name)
    print("took %s seconds" %(train_toc - train_tic))
    print("train sample size N=%s, Epochs=%s"% (len(x),Epochs))
print('Finished Training...')

# --Plot & Save all history of model training.
for model in myModel:
    historia=model.architecture.history.history
    # summarize history for accuracy
    #print(historia)
    fig_acc = plt.figure()
    plt.plot(historia['binary_accuracy'],'o--')
    plt.plot(historia['val_binary_accuracy'],'+--')
    plt.title(f'{model.name}')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    #plt.show()
    #plt.savefig(model.History_files[0])
    fig_acc.savefig(model.History_files[0])
    #fig_acc.close()

    # # summarize history for loss
    fig_loss = plt.figure()
    plt.plot(historia['loss'], 'o--')
    plt.plot(historia['val_loss'], '+--')
    plt.title(f'{model.name}')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    #plt.show()
    fig_loss.savefig(model.History_files[1])
    #fig_loss.close()


