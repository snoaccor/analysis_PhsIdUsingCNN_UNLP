#!/usr/bin/python3


# import sys, getopt, math, os
import sys
import argparse
from array import array
import math
import numpy as np
import ClusterTest_Module
import CNN_Module
# Set up ROOT
import ROOT as r
import tensorflow as tf
from tensorflow.keras.models import load_model
from os import mkdir, listdir
from os.path import join, splitext
print('Loading ROOT magic...')

# --ATLAS Plots Style
def AtlasStyle():
    atlasStyle = r.TStyle("ATLAS","Atlas style")
    # use plain black on white colors
    icol=0 # WHITE
    atlasStyle.SetFrameBorderMode(icol)
    atlasStyle.SetFrameFillColor(icol)
    atlasStyle.SetCanvasBorderMode(icol)
    atlasStyle.SetCanvasColor(icol)
    atlasStyle.SetPadBorderMode(icol)
    atlasStyle.SetPadColor(icol)
    atlasStyle.SetStatColor(icol)
    #atlasStyle.SetFillColor(icol) # don't use: white fill color for *all* objects
    # set the paper & margin sizes
    atlasStyle.SetPaperSize(20,26)

    # set margin sizes
    atlasStyle.SetPadTopMargin(0.07)
    atlasStyle.SetPadRightMargin(0.09)
    atlasStyle.SetPadBottomMargin(0.16)
    atlasStyle.SetPadLeftMargin(0.16)

    # set title offsets (for axis label)
    atlasStyle.SetTitleXOffset(1.4)
    atlasStyle.SetTitleYOffset(1.4)

    # use large fonts
    #Int_t font=72 # Helvetica italics
    font=42 # Helvetica
    tsize=0.05
    atlasStyle.SetTextFont(font)

    atlasStyle.SetTextSize(tsize)
    atlasStyle.SetLabelFont(font,"x")
    atlasStyle.SetTitleFont(font,"x")
    atlasStyle.SetLabelFont(font,"y")
    atlasStyle.SetTitleFont(font,"y")
    atlasStyle.SetLabelFont(font,"z")
    atlasStyle.SetTitleFont(font,"z")

    atlasStyle.SetLabelSize(tsize,"x")
    atlasStyle.SetTitleSize(tsize,"x")
    atlasStyle.SetLabelSize(tsize,"y")
    atlasStyle.SetTitleSize(tsize,"y")
    atlasStyle.SetLabelSize(tsize,"z")
    atlasStyle.SetTitleSize(tsize,"z")

    # use bold lines and markers
    atlasStyle.SetMarkerStyle(20)
    atlasStyle.SetMarkerSize(1.2)
    atlasStyle.SetHistLineWidth(2)
    atlasStyle.SetLineStyleString(2,"[12 12]") # postscript dashes

    # get rid of X error bars
    #atlasStyle.SetErrorX(0.001)
    # get rid of error bar caps
    atlasStyle.SetEndErrorSize(0.)

    # do not display any of the standard histogram decorations
    atlasStyle.SetOptTitle(0)
    #atlasStyle.SetOptStat(1111)
    atlasStyle.SetOptStat(0)
    #atlasStyle.SetOptFit(1111)
    atlasStyle.SetOptFit(0)

    # put tick marks on top and RHS of plots
    atlasStyle.SetPadTickX(1)
    atlasStyle.SetPadTickY(1)
    atlasStyle.SetPalette(1)
    return atlasStyle

def SetAtlasStyle():
    print ("\nApplying ATLAS style settings...")
    atlasStyle = AtlasStyle()
    r.gROOT.SetStyle("ATLAS")
    r.gROOT.ForceStyle()
    return

def setDefaults():
    SetAtlasStyle()
    r.gROOT.SetBatch(True)
    r.gStyle.SetPalette(1)
    return

# --Main class
class Compute_Eff():
    def __init__(self,):
        setDefaults()
        self.args = self.config_Parse
        # # --Global debug flag
        # m_debug = False
        # if self.args.Debug:
            # self.m_debug = True
        # --Set filetype
        # assert self.args.FType == ('EGAM3' or 'xAOD'), 'unsuported type, enter "EGAM3" or "xAOD" '
        # self.filetype = self.args.FType

        # --Folder to store the Output files.
        self.OutputFolder = 'Efficiencies_figures'
        print(f'--> Output files will be store in folder {self.OutputFolder}')
        try:
            mkdir(self.OutputFolder)
        except: pass

    @property
    def config_Parse(self):
        parser = argparse.ArgumentParser('Compute_Eff_parser')
        # -- Set default values
        _pTbins_default =[10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 52, 54, 56, 58, 60, 62, 64, 66, 68, 70, 75, 80]
        _etabins_default =[ -2.37, -2.01, -1.81, -1.52, -1.37, -1.15, -0.8 , -0.6 , 0    , 0.6  , 0.8  , 1.15 , 1.37 , 1.52 , 1.81 , 2.01 , 2.37]
        # -- Config parser
        parser.add_argument('-f', '--File', required=True, help='CellsDumping output file' )
        parser.add_argument('-m', '--ModelFolder', required=True, help='Model.h5 to retrieve the trained NN model.')
        parser.add_argument('-D','--Debug', required=False, help='Debug flag', action='store_true')
        parser.add_argument('-t','--FType',required=False, help='Type of TFile, EGAM3 or xAOD.')
        parser.add_argument('-C','--CacheFile', required=False, help='Output file name', default='Cache.root')
        parser.add_argument('-M','--MaxEvents', required=False, help='Set maximum of events. Default -1 == all', type=int, default=-1)
        parser.add_argument('-e','--EtaBins', required=False, help='Eta Bins', type=float,
                default=_etabins_default )
        parser.add_argument('-p','--PtBins', required=False, help='Pt Bins', type=float,
                default=_pTbins_default )

        args = parser.parse_args()

        # --Assert inputs
        assert args.FType == ('EGAM3' or 'xAOD'), 'unsuported type, enter "EGAM3" or "xAOD" '
        assert type(args.ModelFolder)==str, 'pass a file path'

        return args

    # def Load_Models(ModelsSavedFolder, h_num_cls):
    def load_Models(self, ModelsSavedFolder, h_num_cls, load_fmt='LOCAL',model_name=None,path_to_model_wgts=None):
        # TODO: add option to load model from json
        '''
        # Args:
            load_fmt: format for loading files. Options are:
                - 'LOCAL': uses models define in CNN_Module, and weights saved in .h5 files.
                - 'WEIGHTS': needs a model.json file with the architecture and a .h5 file having the weights.
                - 'FULL': uses TensorFlow SavedModel format.
        # Returns:
            list containing models object
        '''
        ModelsLoaded=[]
        if load_fmt == 'LOCAL':
            for files in listdir(ModelsSavedFolder):
                # print(files)
                # --check the files extentions is correct
                f_name, ext = splitext(files)
                assert ext == '.h5'
                # --Reload the trained model
                model_arch = load_model(join(ModelsSavedFolder,files))
                # --Set name
                model_name = f_name.split('_')[1]
                print(f'Model Name: {model_name}')
                h_num_cls.append(model_name)

                # --store all data in MyModels object
                ModelsLoaded.append(
                        CNN_Module.MyModels(
                            model_name,
                            architecture=model_arch
                            )
                        )
        elif load_fmt == 'WEIGHTS':
            json_path = join(ModelsSavedFolder,'model.json')
            with open(json_path,'r') as json_file:
                loaded_from_json = json_file.read()
                model_arch = tf.keras.models.model_from_json(loaded_from_json)
                model_arch.load_weights(path_to_model_wgts)

            ModelsLoaded.append(
                    CNN_Module.MyModels(
                        model_name,
                        architecture=model_arch
                        )
                    )
        else:
            print('***** NOT IMPLEMENTED YET *****')
        print(f'-->All models Reloaded!!')
        return ModelsLoaded

    def set_Cuts(self,data,ph):
        """
        Description

        # Args::

            data: (TTree),

            ph: (Photon obj)

        # Returns::

            Cut_res: (dict), holds the result of applying various cuts to given ph.
        """
        Cut_res = {}

        Cut_res['eta'] = abs(data.Phs_eta[ph]) < 1.37
        Cut_res['mass'] = (40000< data.ee_m[ph] <83000) and (80000< data.Z_m[ph] <100000)
        Cut_res['tight'] = data.Phs_Tight[ph] > 0
        Cut_res['L2HLT'] = PassSelectionL2HLT(
                threshold=25,
                ph_eta=data.Phs_eta[ph],
                ph_pT=data.Phs_pt[ph],
                ph_Reta=data.Ph_SS_Reta[ph],
                ph_Rhad=data.Ph_SS_Rhad[ph],
                ph_Eratio=data.Ph_SS_Eratio[ph],
                ph_F1=data.Ph_SS_f1[ph]
                )

        # --ZRad Denominator condition
        Cut_res['IsGoodPhotonZRadDenominator'] = Cut_res['eta']  and Cut_res['mass'] and data.PZrad[ph] > 0
        # --Bkg Denominator condition
        Cut_res['IsGoodPhotonBkgDenominator']=  Cut_res['eta'] and data.Ph_TopoEtCone20[ph] > 0.07

        return Cut_res

    # def get_models_predictions(ModelsLoaded,Image,Histo_predict, IsGoodPhotonZRadDenominator, IsGoodPhotonBkgDenominator,Eff_cut=0.5):
    def get_models_predictions(self,ModelsLoaded,Image,Histo_predict, IsGoodPhotonZRadDenominator, IsGoodPhotonBkgDenominator,Eff_cut=0.5):
        """
        Description

        # Args::

        # Returns::

            Im_predict: (dict), holds perdiction results for each model.

            PassSelectionCNN: (dict),
        """
        Im_predict={}
        PassSelectionCNN={}
        for model in ModelsLoaded:
            # --Do and save the prediction made by the model on an Image.
            Im_predict[model.name] = model.architecture.predict(Image)
            # --Check if the ph passed the CNN selection
            PassSelectionCNN[model.name], CNN_uncertainty = ClusterTest_Module.PassSelectionCNN(
                    Im_predict[model.name],
                    Eff_cut=0.5
                    )
            # --Fill histogram of prediction for each model.
            if IsGoodPhotonZRadDenominator:
                Histo_predict[model.name]['T'].Fill(Im_predict[model.name])
            elif IsGoodPhotonBkgDenominator:
                Histo_predict[model.name]['F'].Fill(Im_predict[model.name])

        # yield Im_predict, PassSelectionCNN
        return Im_predict, PassSelectionCNN

# --Subroutines
def createT1(name, bins):
    """
    # Parameters:
        name: name of the Historgram

        bins: array of bin steps.

    # Returns:
        ROOT.TH1F histogram
    """
    binarray = array('d', bins)
    h = r.TH1F(name,name,len(bins),bins[0],bins[-1])
    h.SetBins(len(bins)-1, binarray)
    return h

def Set_histograms(magnitudes, h_den_cls, h_num_cls, binarray):
    """
    Create a dict filled with empty histograms classified for magnitudes, role, and class.

    # Args::
         magnitudes:
         h_den_cls:
         h_num_cls:
         binarray:

    # Returns::
        histograms: dict full of TH1F histograms built using createT1.
        The keys classify them into magnitud, den or num, and class.
    """
    histograms={}
    for magn in magnitudes:
        histograms[magn]={}
        #{histograms[magn][h_ty] for h_ty in h_types}
        histograms[magn]['den']={}
        histograms[magn]['num']={}
        # -- create den histograms
        for cls in h_den_cls:
            h_name = 'h_'+magn+'_den'+cls
            new_histo = createT1(h_name,binarray[magn])
            histograms[magn]['den'][cls] = new_histo
            print(f'> histograms: {new_histo.GetName()} created')

        for cls in h_num_cls:
            histograms[magn]['num'][cls]={}
            for den in h_den_cls:
                h_name = 'h_'+magn+'_num'+cls+'VS'+den
                new_histo = createT1(h_name,binarray[magn])
                histograms[magn]['num'][cls][den] = new_histo

                print(f'> histograms: {new_histo.GetName()} created')
    return histograms

def Fill_Histo(histogram, magnitud, data, ph):
    if magnitud == 'pt':
        histogram.Fill(data.Phs_pt[ph]/1000.)
    if magnitud == 'eta':
        histogram.Fill(data.Phs_eta[ph])
    #return histogram
    return

def get_EffHistos(Eff_Histo, Eff_name, Num_histogram, Den_histogram):
    Eff_Histo.SetName(Eff_name)
    Eff_Histo.SetTitle(Eff_name)
    xvar = Eff_name.split('_')[1]
    Eff_Histo.Divide(
        Num_histogram,
        Den_histogram,
        'cl=0.683 b(0.5,0.5) mode'
        )
    #en Xaxis has to go either eta or pT
    Eff_Histo.GetXaxis().SetTitle(xvar)
    # if xvar=='pT': Eff_Histo.GetXaxis.SetTitle('pT [GeV]')
    Eff_Histo.GetYaxis().SetTitle('Eficiencias')
    Eff_Histo.GetYaxis().SetRangeUser(0.0,1.1)
    Eff_Histo.Write(Eff_name)
    return Eff_Histo

def Set_Eff(Eff_Dict, histograms, magnitudes, h_den_cls, h_num_cls):
    for magn in magnitudes:
        for den in h_den_cls:
            Eff_Dict[magn][den]={}
            for num in h_num_cls:
                Eff_name = 'Eff_'+magn+'_'+num+'VS'+den
                #Eff_TightVsZRad_Pt = r.TGraphAsymmErrors()
                Eff_Histo = r.TGraphAsymmErrors()
                num_histo = histograms[magn]['num'][num][den]
                den_histo = histograms[magn]['den'][den]
                # print(f'num:{num_histo.GetName()}, den:{den_histo.GetName()}')
                #print(Eff_Dict[magn][den][num])
                Eff_Histo = get_EffHistos(
                        Eff_Histo,
                        Eff_name,
                        num_histo, #histograms[magn]['num'][num][den],  #h_numTIGHTvsZRAD_pt,
                        den_histo, #histograms[magn]['den'][den]        #h_denZRad_pti
                        )
                Eff_Dict[magn][den][num] = Eff_Histo
    print(f'-- All Efficiencies computed --')
    return Eff_Dict

def DrawAndSave(Eff_Dict, ROC_Dict, SaveFolder, FileFormat = 'eps'):
    # print(Eff_Dict)
    # --since we are ploting 4 curves at a time
    Colour = [1, 2, 3, 4]
    Style = ['AZEP','EP','EP','EP']
    # --Set legend format
    for magn in Eff_Dict.keys():    #either pT or eta
        canvas_last = magn
        for den in Eff_Dict[magn].keys():  #either zrad or bkg
            canvas_first = 'c_' + den
            canvas_name = canvas_first + canvas_last
            # --Set TCanvas
            TCanvas = r.TCanvas(canvas_name)
            TCanvas.cd()
            # print(TCanvas.GetName())
            legend = r.TLegend(0.2,0.5,0.2,0.5)
            legend.SetFillStyle(3000)
            legend.SetFillColor(0)
            legend.SetBorderSize(0)
            legend.SetTextFont(52)

            # --loop through histograms
            for num, histo, color, style in zip(
                    Eff_Dict[magn][den].keys(),
                    Eff_Dict[magn][den].values(),
                    Colour,
                    Style):
                histo.SetMarkerColor(color)
                histo.SetLineColor(color)
                #histo.SetMarkerStyle(M_styles[j])
                histo.SetMarkerSize(1)
                histo.Draw(style)
                legend_name = histo.GetName().split('_')[2]
                legend_name = legend_name.split('VS')[0]
                legend.AddEntry(histo, legend_name,'p')

            legend.Draw()
            print(f' Historgrams: being saved.')
            #gPad.BuildLegend()
            Outputfile_name = join(
                    SaveFolder,
                    f'Eff_vs_{str(den)}_{str(magn)}.{FileFormat}'
                    )
            TCanvas.Draw()
            TCanvas.SaveAs(Outputfile_name)

    for model in ROC_Dict.keys():
        TCanvas = r.TCanvas()
        legend = r.TLegend(0.2,0.5,0.2,0.5)
        legend.SetFillStyle(3000)
        legend.SetFillColor(0)
        legend.SetBorderSize(0)
        legend.SetTextFont(52)
        for cond,histo in ROC_Dict[model].items():
            histo.SetMarkerColor(1)
            histo.SetLineColor(1)
            histo.SetMarkerSize(1)
            histo.Draw('AZEP')
            legend_name = histo.GetName()#.split('_')[2]
            # legend_name = legend_name.split('VS')[0]
            legend.AddEntry(histo, legend_name,'p')

            legend.Draw()
            print(f' Historgram: being saved.')
            #gPad.BuildLegend()
            Outputfile_name = join(
                    SaveFolder,
                    f'predict_histo_{model}_{cond}.{FileFormat}'
                    )
            TCanvas.Draw()
            TCanvas.SaveAs(Outputfile_name)
    return

# def fill_all_histo(data,ph,m_histos,
        # IsGoodPhotonZRadDenominator,IsGoodPhotonBkgDenominator,
        # PassSelectionTight,PassSelectionCNN):
def fill_all_histo(data,ph,m_histos,
        IsGoodPhotonZRadDenominator,IsGoodPhotonBkgDenominator,
        PassSelectionTight, PassSelectionL2HLT,
        PassSelectionCNN):
    '''
    Fill all the necessary histograms to compute Efficiencies.
    '''
    # --Fill Histograms vs ZRad
    if IsGoodPhotonZRadDenominator:
        Fill_Histo(m_histos['eta']['den']['zrad'], 'eta', data, ph)
        Fill_Histo(m_histos['pt']['den']['zrad'], 'pt', data, ph)
    if IsGoodPhotonZRadDenominator and PassSelectionTight:
        Fill_Histo(m_histos['eta']['num']['tight']['zrad'], 'eta', data, ph)
        Fill_Histo(m_histos['pt']['num']['tight']['zrad'], 'pt', data, ph)
    if IsGoodPhotonZRadDenominator and PassSelectionL2HLT:
        Fill_Histo(m_histos['eta']['num']['L2HLT']['zrad'], 'eta', data, ph)
        Fill_Histo(m_histos['pt']['num']['L2HLT']['zrad'], 'pt', data, ph)
    for model_name in PassSelectionCNN.keys():
        if IsGoodPhotonZRadDenominator and PassSelectionCNN[model_name]:
            Fill_Histo(m_histos['eta']['num'][model_name]['zrad'], 'eta', data, ph)
            Fill_Histo(m_histos['pt']['num'][model_name]['zrad'], 'pt', data, ph)
    # --Fill Histograms vs Bkg
    if IsGoodPhotonBkgDenominator:
        Fill_Histo(m_histos['eta']['den']['bkg'], 'eta', data, ph)
        Fill_Histo(m_histos['pt']['den']['bkg'], 'pt', data, ph)
    if IsGoodPhotonBkgDenominator and PassSelectionTight:
        Fill_Histo(m_histos['eta']['num']['tight']['bkg'], 'eta', data, ph)
        Fill_Histo(m_histos['pt']['num']['tight']['bkg'], 'pt', data, ph)
    if IsGoodPhotonBkgDenominator and PassSelectionL2HLT:
        Fill_Histo(m_histos['eta']['num']['L2HLT']['bkg'], 'eta', data, ph)
        Fill_Histo(m_histos['pt']['num']['L2HLT']['bkg'], 'pt', data, ph)
    for model_name in PassSelectionCNN.keys():
        if IsGoodPhotonBkgDenominator and PassSelectionCNN[model_name]:
            Fill_Histo(m_histos['eta']['num'][model_name]['bkg'], 'eta', data, ph)
            Fill_Histo(m_histos['pt']['num'][model_name]['bkg'], 'pt', data, ph)
    return

def PassSelectionL2HLT(threshold, ph_eta, ph_pT, ph_Reta, ph_Rhad, ph_Eratio, ph_F1):
    etaBins = [0, 0.6, 0.8, 1.15, 1.37, 1.52, 1.81, 2.01, 2.37, 2.47]
    # --Set trigger cuts for eta bins according to a given threshold
    #   for now it's only Medium Cuts

    # TODO: find where are F1 cuts define
    #       I think is here: https://gitlab.cern.ch/atlas/athena/-/blob/21.1/Trigger/TrigHypothesis/TrigEgammaHypo/python/TrigL2PhotonHypoConfig.py#L30
    trigger_F1Cut = [0.005]
    if (20. <= threshold < 30):
        trigger_RhadCut= [0.071, 0.062, 0.075, 0.060, 0.051, 0.057, 0.075, 0.072, 0.051]
        trigger_RetaCut = [0.819375, 0.819375, 0.800375, 0.828875, 0.7125, 0.805125, 0.843125, 0.824125, 0.700625]
        trigger_EratioCut =[-999., -999., -999., -999., -999., -999., -999., -999., -999.]

    if (30. <= threshold < 40):
        trigger_RhadCut= [0.071, 0.062, 0.075, 0.060, 0.051, 0.057, 0.075, 0.072, 0.051]
        trigger_RetaCut = [0.819375, 0.819375, 0.800375, 0.828875, 0.7125, 0.805125, 0.843125, 0.824125, 0.700625]
        trigger_EratioCut =[-999., -999., -999., -999., -999., -999., -999., -999., -999.]

    # -- Get etabin given ph_eta
    _doLoop=True
    i=0
    iBin=0
    # for i in range(len(etaBins)-1):
    while _doLoop:
        if etaBins[i] <= ph_eta < etaBins[i+1]:
            iBin = i
            _doLoop = False
        elif i < len(etaBins)-1:
            i+=1
        else:
            # print(f'Exiting loop')
            _doLoop= False

    # print(f'testing bin {iBin}')

    # -- Apply cuts to Eratio, Rhad, Reta, F1, DeltaEta, DeltaPhi

    # DEPRECATED ?? check line 356 of TrigL2PhotonHypo
    # --Apply F1 cut
    # F1Cut = ph_F1 <

    # --Apply Reta cut
    RetaCut = ph_Reta >= trigger_RetaCut[iBin]

    # --Apply Rhad cut
    # print(f'Rhad: {ph_Rhad}, TrigCut: {trigger_RhadCut[iBin]},')
    # if ph_pT !=0:
        # HadEmRatio = ph_Rhad / ph_pT
    # else:
        # HadEmRatio = -1.
    RhadCut = ph_Rhad <= trigger_RhadCut[iBin]
    # RhadCut = HadEmRatio > trigger_RhadCut[iBin]

    # --Apply Eratio cut
    # TODO: this seems to be not working
    inCrack = abs(ph_eta) > 2.37 or (1.37 < abs(ph_eta) and abs(ph_eta) < 1.52)
    # if (inCrack or (ph_F1 < trigger_F1Cut[0])):
    if inCrack:
        # print('Eratio cut could not been applied')
        EratioCut = False
    else:
        EratioCut = ph_Eratio >= trigger_EratioCut[iBin]

    # print(f'Reta: {RetaCut}, Eratio: {EratioCut}, Rhad: {RetaCut}')
    # PassCond = RetaCut and EratioCut and RhadCut# and F1Cut
    PassCond = RetaCut and EratioCut and RhadCut# and F1Cut
    # print(f'testing PassCond: {PassCond}')
    return PassCond

def main(argv):
    instance = Compute_Eff()
    args = instance.config_Parse

    # --Set parameters
    binarray_Pt = array('d', args.PtBins)
    binarray_Eta = array('d', args.EtaBins)
    binarray_predict = array('d',np.arange(0,1,.05))

    binarray = {'eta':binarray_Eta, 'pt':binarray_Pt}
    magnitudes = ('eta','pt')
    h_types = ('num','den')
    h_num_cls = ['tight','L2HLT'] #++ models name
    # h_num_cls = ['tight'] #++ models name
    h_den_cls = ('zrad','bkg')

    # --Set TTree to store all output
    tree_out = r.TTree('eval_cnn_out','eval_cnn_out')

    # --Define var to fill in TTree
    v_pT=array('f',[0])
    v_eta=array('f',[0])
    v_zrad=array('f',[0])
    v_Bkg=array('f',[0])
    v_tight=array('f',[0])
    v_L2HLT=array('f',[0])
    # v_cnnmodel0predict=array('f',[0])
    # v_cnnmodel1predict=array('f',[0])
    v_cnnMCpredict=array('f',[0])

    tot_corrupt_clus=0

    tree_out.Branch('v_pT',v_pT,'v_pT/F')
    tree_out.Branch('v_eta',v_eta,'v_eta/F')
    tree_out.Branch('v_zrad',v_zrad,'v_zrad/F')
    tree_out.Branch('v_Bkg',v_Bkg,'v_Bkg/F')
    tree_out.Branch('v_tight',v_tight,'v_tight/F')
    tree_out.Branch('v_L2HLT',v_L2HLT,'v_L2HLT/F')
    # tree_out.Branch('v_cnnmodel0predict',v_cnnmodel0predict,'v_cnnmodel0predict/F')
    # tree_out.Branch('v_cnnmodel1predict',v_cnnmodel1predict,'v_cnnmodel1predict/F')
    tree_out.Branch('v_cnnMCpredict',v_cnnMCpredict,'v_cnnMCpredict/F')

    # --Load model
    # ModelsLoaded = instance.load_Models(args.ModelFolder, h_num_cls)
    ModelsLoaded = instance.load_Models(args.ModelFolder, h_num_cls,load_fmt='WEIGHTS',model_name='MC_trainCNN',path_to_model_wgts=join(args.ModelFolder,'weights.Ep20-val_loss0.51.h5'))

    RootOutFile = r.TFile('EffHistos.root','recreate')

    # Create TChain
    data = r.TChain('myTree_Noacco')
    data.Add(args.File)

    # denominator (den) is ph passed ZRAD.
    # numerators (num) are classified as ph by CNN and
    # classified as Tight.


    # one entry per sample
    print('INFO:: Processing sample ')

    # -- Set histograms
    m_histos = Set_histograms(magnitudes, h_den_cls, h_num_cls, binarray)
    histo_predict={}
    for models in ModelsLoaded:
        histo_predict[models.name]={}
        histo_predict[models.name]['T']=createT1('TPhisto',bins=binarray_predict)
        histo_predict[models.name]['F']=createT1('FPhisto',bins=binarray_predict)



    # Now, try to get cached efficiencies computed in the outputfile
    AllCached=True


    # Open output cache file to store results
    #cachefile = r.TFile(args.CacheFile,'update')

    # --Start loop
    Events = data.GetEntries()
    MaxEvents = Events
    if (args.MaxEvents > 0):
        MaxEvents = min(Events,args.MaxEvents)

    #-- Main Loop
    for ev in range(MaxEvents):
        # --Get event data.
        data.GetEntry(ev)
        # if ev%1000==0 or m_debug:
        if ev%1000==0 or args.Debug:
            print(f'Processing event {str(ev)}...')
        # first check if photon passes denominator
        for ph in range(len(data.PZrad)):
            # --We are interested on ph above 20GeV cause its trigger.
            if data.Phs_pt[ph] < 20000: continue

            # --Get results from applying cuts to phs
            cuts_res = instance.set_Cuts(data,ph)

            # --Get the images of the TopoCluster of the photon.
            Image, skipcond = ClusterTest_Module.Get_Images(
                    TTree=data,
                    Filetype=args.FType,
                    numEv=ev,
                    DEBUG=args.Debug
                    )
            if skipcond:
                tot_corrupt_clus+=1
                if args.Debug:
                    print('skiping corrupted cluster')
                continue
            #print(Image)

            # NOTE: to use an external model with a different input shape,
            #       first transpose the Images
            do_reshape=True
            if do_reshape:
                Image = np.transpose(Image)
                # print(Image.shape)
                # DOUBT how is this resize working on the image? what does it chop?
                Image = np.resize(Image,(1,11,7,1))
                # print(Image.shape)


            # --Use the model to predict the outcome
            Im_predict, PassSelectionCNN = instance.get_models_predictions(
                            ModelsLoaded,
                            Image,
                            histo_predict,
                            cuts_res['IsGoodPhotonZRadDenominator'],
                            cuts_res['IsGoodPhotonBkgDenominator']
                            )


            # --save data in branches
            v_pT[0]=data.Phs_pt[ph]
            v_eta[0]=data.Phs_eta[ph]
            v_zrad[0]=cuts_res['IsGoodPhotonZRadDenominator']
            v_Bkg[0]=cuts_res['IsGoodPhotonBkgDenominator']
            v_tight[0]=data.Phs_Tight[ph]
            v_L2HLT[0]=cuts_res['L2HLT']
            # v_cnnmodel0predict[0]=Im_predict['model0'][0]
            # v_cnnmodel1predict[0]=Im_predict['model1'][0]
            v_cnnMCpredict[0]=Im_predict['MC_trainCNN'][0]

            # print(f'y_true:{cuts_res["L2HLT"]}, y_pred={Im_predict["MC_trainCNN"][0]}')

            # --Fill TTree, must be here!!
            tree_out.Fill()

            # --Fill Histograms
            # fill_all_histo(
                    # data,ph,m_histos,
                    # cuts_res['IsGoodPhotonZRadDenominator'],cuts_res['IsGoodPhotonBkgDenominator'],
                    # cuts_res['tight'],PassSelectionCNN
                    # )
            fill_all_histo(
                    data,ph,m_histos,
                    cuts_res['IsGoodPhotonZRadDenominator'],cuts_res['IsGoodPhotonBkgDenominator'],
                    cuts_res['tight'],cuts_res['L2HLT'],
                    PassSelectionCNN
                    )

    tree_out.Write()

    # This dictionary will store ALL efficiencies
    EffDict={}
    EffDict['eta']={}
    EffDict['pt']={}

    # print(f'histograms : {m_histos}')
    Set_Eff(EffDict, m_histos, magnitudes, h_den_cls, h_num_cls)
    DrawAndSave(EffDict,histo_predict, instance.OutputFolder)

    RootOutFile.Close()

if __name__ == "__main__":
    main(sys.argv[1:])

