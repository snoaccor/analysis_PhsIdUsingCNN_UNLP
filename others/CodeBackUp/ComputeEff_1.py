#!/usr/bin/python
# Correlo sin argumentos y ves como se corre
# Por ejemplo, yo lo corri asi:
# /ComputeEff.py -f EGAM3_InputCNN/Merged.user.fernando.data18_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_EGAM3.grp18_v01_p3948_2_MYSTREAM_\* --MaxEvents 100000

import sys, getopt, math, os
import argparse
import array
import math
import numpy as np
import ClusterTest_Module 
<<<<<<< HEAD
from numpy import arangei
from tensorflow.keras import load_model
=======
<<<<<<< HEAD:src/Clusters/ComputeEff.py
import CNN_Module 
#from numpy import arange
#import tensorflow.keras
from tensorflow.keras.models import load_model
=======
from numpy import arangei
from tensorflow.keras import load_model
>>>>>>> 31044d4fd4b3f271a73b5a3dd4dc76e9c8099d86:src/CodeBackUp/ComputeEff_1.py
>>>>>>> 31044d4fd4b3f271a73b5a3dd4dc76e9c8099d86

print('Loading ROOT magic...')

# Set up ROOT and RootCore:
import ROOT as r
<<<<<<< HEAD
def SetAtlasStyle ():
=======
def SetAtlasStyle():
>>>>>>> 31044d4fd4b3f271a73b5a3dd4dc76e9c8099d86
    print ("\nApplying ATLAS style settings...")
    atlasStyle = AtlasStyle()
    r.gROOT.SetStyle("ATLAS")
    r.gROOT.ForceStyle()

def AtlasStyle():
    atlasStyle = r.TStyle("ATLAS","Atlas style")
    # use plain black on white colors
    icol=0 # WHITE
    atlasStyle.SetFrameBorderMode(icol)
    atlasStyle.SetFrameFillColor(icol)
    atlasStyle.SetCanvasBorderMode(icol)
    atlasStyle.SetCanvasColor(icol)
    atlasStyle.SetPadBorderMode(icol)
    atlasStyle.SetPadColor(icol)
    atlasStyle.SetStatColor(icol)
    #atlasStyle.SetFillColor(icol) # don't use: white fill color for *all* objects
    # set the paper & margin sizes
    atlasStyle.SetPaperSize(20,26)

    # set margin sizes
    atlasStyle.SetPadTopMargin(0.07)
    atlasStyle.SetPadRightMargin(0.09)
    atlasStyle.SetPadBottomMargin(0.16)
    atlasStyle.SetPadLeftMargin(0.16)

    # set title offsets (for axis label)
    atlasStyle.SetTitleXOffset(1.4)
    atlasStyle.SetTitleYOffset(1.4)

    # use large fonts
    #Int_t font=72 # Helvetica italics
    font=42 # Helvetica
    tsize=0.05
    atlasStyle.SetTextFont(font)

    atlasStyle.SetTextSize(tsize)
    atlasStyle.SetLabelFont(font,"x")
    atlasStyle.SetTitleFont(font,"x")
    atlasStyle.SetLabelFont(font,"y")
    atlasStyle.SetTitleFont(font,"y")
    atlasStyle.SetLabelFont(font,"z")
    atlasStyle.SetTitleFont(font,"z")

    atlasStyle.SetLabelSize(tsize,"x")
    atlasStyle.SetTitleSize(tsize,"x")
    atlasStyle.SetLabelSize(tsize,"y")
    atlasStyle.SetTitleSize(tsize,"y")
    atlasStyle.SetLabelSize(tsize,"z")
    atlasStyle.SetTitleSize(tsize,"z")

    # use bold lines and markers
    atlasStyle.SetMarkerStyle(20)
    atlasStyle.SetMarkerSize(1.2)
    atlasStyle.SetHistLineWidth(2)
    atlasStyle.SetLineStyleString(2,"[12 12]") # postscript dashes

    # get rid of X error bars 
    #atlasStyle.SetErrorX(0.001)
    # get rid of error bar caps
    atlasStyle.SetEndErrorSize(0.)

    # do not display any of the standard histogram decorations
    atlasStyle.SetOptTitle(0)
    #atlasStyle.SetOptStat(1111)
    atlasStyle.SetOptStat(0)
    #atlasStyle.SetOptFit(1111)
    atlasStyle.SetOptFit(0)

    # put tick marks on top and RHS of plots
    atlasStyle.SetPadTickX(1)
    atlasStyle.SetPadTickY(1)
    atlasStyle.SetPalette(1)
    return atlasStyle

def setDefaults():
    SetAtlasStyle()
    r.gROOT.SetBatch(True)
    r.gStyle.SetPalette(1)

def CreateT1(name, bins):
    binarray = array.array('d', bins)
    h = r.TH1F(name,name,len(bins),bins[0],bins[-1])
    h.SetBins(len(bins)-1, binarray)
    return h

def main(argv):
    
    parser = argparse.ArgumentParser('Trigger Efficiency from TF')
    parser.add_argument('-f', '--File', required=True, help='CellsDumping output file' )
    parser.add_argument('-t','--FType',required=True, help='Type of TFile, EGAM3 or xAOD.')
    parser.add_argument('-m', '--Model', required=True, help='Model.h to retrieve the trained NN model.')
    parser.add_argument('-D','--Debug', required=False, help='Debug flag', action='store_true')
    parser.add_argument('-C','--CacheFile', required=False, help='Output file name', default='Cache.root')
    parser.add_argument('-M','--MaxEvents', required=False, help='Set maximum of events. Default -1 == all', type=int, default=-1)
    parser.add_argument('-e','--EtaBins', required=False, help='Eta Bins', type=float, default=[ -2.37, -2.01, -1.81, -1.52, -1.37, -1.15, -0.8 , -0.6 , 0    , 0.6  , 0.8  , 1.15 , 1.37 , 1.52 , 1.81 , 2.01 , 2.37] )
    parser.add_argument('-p','--PtBins', required=False, help='Pt Bins', type=float, default= [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 52, 54, 56, 58, 60, 62, 64, 66, 68, 70, 75, 80])

    args = parser.parse_args()

    # Global debug flag
    m_debug = False
    if args.Debug:
        m_debug=True
    
    # Set filetype
    assert args.FType == ('EGAM3' or 'xAOD'), 'unsuported type, enter "EGAM3" or "xAOD" '
    filetype = args.FType
    
    assert type(args.Model)==str, 'pass a file path'
    ext = os.path.splitext(args.Model)[1]
    assert ext == 'h'
    model = load_model(args.Model)
<<<<<<< HEAD
=======
<<<<<<< HEAD:src/Clusters/ComputeEff.py
    
=======
>>>>>>> 31044d4fd4b3f271a73b5a3dd4dc76e9c8099d86
    # --Compile the model to use it.
    model.compile(loss='binary_crossentropy',
                 optimizer=RMSprop(lr=0.0001),
                 metrics=['accuracy'])
<<<<<<< HEAD
=======
>>>>>>> 31044d4fd4b3f271a73b5a3dd4dc76e9c8099d86:src/CodeBackUp/ComputeEff_1.py
>>>>>>> 31044d4fd4b3f271a73b5a3dd4dc76e9c8099d86

    # This dictionary will store ALL efficiencies
    EffDict={}
    EffDict['eta']={}
    EffDict['pt']={}
    EffDict['mu']={}

    # Create TChain

    data = r.TChain('myTree_Noacco')
    data.Add(args.File)

    # denominator (den) is ph passed ZRAD.
    # numerators (num) are classified as ph by CNN and
    # classified as Tight.


    # one entry per sample
    print('INFO:: Processing sample ')


    binarray_Pt = array.array('d', args.PtBins)
    binarray_Eta = array.array('d', args.EtaBins)
    # Lets define num, den and eff for eta
<<<<<<< HEAD
    h_num_eta = CreateT1('num_eta',binarray_Eta)
    h_den_eta = CreateT1('den_eta',binarray_Eta)

    # Lets define num, den and eff for pt
    h_num_pt = CreateT1('num_pt',binarray_Pt)
=======
    h_numTight_eta = CreateT1('num_eta',binarray_Eta)
    h_numCNN_eta = CreateT1('num_eta',binarray_Eta)
    h_den_eta = CreateT1('den_eta',binarray_Eta)

    # Lets define num, den and eff for pt
    h_numTight_pt = CreateT1('num_pt',binarray_Pt)
    h_numCNN_pt = CreateT1('num_pt',binarray_Pt)
>>>>>>> 31044d4fd4b3f271a73b5a3dd4dc76e9c8099d86
    h_den_pt = CreateT1('den_pt',binarray_Pt)


    # Now, try to get cached efficiencies computed in the outputfile
    AllCached=True


    # Start loop
    Events = data.GetEntries()

    MaxEvents = Events
    if (args.MaxEvents > 0):
        MaxEvents = min(Events,args.MaxEvents)


    # Open output cache file to store results
    cachefile = r.TFile(args.CacheFile,'update')

    for ev in range(MaxEvents):
        # --Get event data.
        data.GetEntry(ev)
        
<<<<<<< HEAD
        # --Set some variables
        isTight = data.Phs_Tight    #TODO: implement this instead of line 186 

=======
<<<<<<< HEAD:src/Clusters/ComputeEff.py
=======
        # --Set some variables
        isTight = data.Phs_Tight    #TODO: implement this instead of line 186 

>>>>>>> 31044d4fd4b3f271a73b5a3dd4dc76e9c8099d86:src/CodeBackUp/ComputeEff_1.py
>>>>>>> 31044d4fd4b3f271a73b5a3dd4dc76e9c8099d86
        if ev%1000==0 or m_debug:
            print('Processing event ' + str(ev) + '...')
  
        # first check if photon passes denominator
        for ph in range(len(data.Phs_Tight)):
            IsGoodPhotonDenominator = data.PZrad[ph] > 0 
            PassSelectionTight = data.Phs_Tight[ph] > 0 
            
            # PassSelectionCNN = Aca Evaluar el modelo en las celdas!
<<<<<<< HEAD
            Image, skipcond = ClusterTest_Module.Get_Images(
                    TTree=data,
=======
            # --Get the images of the TopoCluster of the photon.
            Image, skipcond = ClusterTest_Module.Get_Images(
                    TTree=data,
<<<<<<< HEAD:src/Clusters/ComputeEff.py
                    Filetype=filetype,
                    numEv=ev,
                    DEBUG=m_debug
                    )
            if skipcond: 
                if m_debug:
                    print('skiping corrupted cluster')
                continue
            #print(Image)
            
            # --Use the model to predict the outcome
            res_Im = model.predict(Image)
            #print(res_Im) 
            # --TODO: make a function to set is Signal when
            #   when CNN predicts arround 1, also compute its 
            #   uncertainty.

            # --Check if the ph passed the CNN selection
            PassSelectionCNN, CNN_uncertainty = ClusterTest_Module.PassSelectionCNN(res_Im,Eff_cut=0.25)
            #print(f'Selection : {PassSelectionCNN}, Uncertainty {CNN_uncertainty} ')

=======
>>>>>>> 31044d4fd4b3f271a73b5a3dd4dc76e9c8099d86
                    Filetype= iletype,
                    numEv=ev
                    )
            if skipcond: continue

            # --Use the model to predict the outcome
            res_Im = model.predict(Image)
            print(res_Im) 
<<<<<<< HEAD
=======
>>>>>>> 31044d4fd4b3f271a73b5a3dd4dc76e9c8099d86:src/CodeBackUp/ComputeEff_1.py
>>>>>>> 31044d4fd4b3f271a73b5a3dd4dc76e9c8099d86
            if IsGoodPhotonDenominator:
                h_den_pt.Fill(data.Phs_pt[ph]/1000.)
                h_den_eta.Fill(data.Phs_eta[ph])
    
    
            if IsGoodPhotonDenominator and PassSelectionTight:
<<<<<<< HEAD
                h_num_pt.Fill(data.Phs_pt[ph]/1000.)
                h_num_eta.Fill(data.Phs_eta[ph])
    

    # Compute efficiencies with processed events
    Eff_pt = r.TGraphAsymmErrors()
    Eff_pt.SetName('Eff_pt')
    Eff_pt.SetTitle('Eff_pt')
    Eff_pt.Divide(h_num_pt,h_den_pt,'cl=0.683 b(0.5,0.5) mode')
    Eff_pt.Write()



    Eff_eta = r.TGraphAsymmErrors()
    Eff_eta.SetName('Eff_eta')
    Eff_eta.SetTitle('Eff_eta')
    Eff_eta.Divide(h_num_eta,h_den_eta,'cl=0.683 b(0.5,0.5) mode')
    Eff_eta.Write()


    # Plot
    c = r.TCanvas('Canvas')
    Eff_pt.Draw('AZEP')
    c.SaveAs('EffPt.eps')

    Eff_eta.Draw('AZEP')
    c.SaveAs('EffEta.eps')
=======
<<<<<<< HEAD:src/Clusters/ComputeEff.py
                h_numTight_pt.Fill(data.Phs_pt[ph]/1000.)
                h_numTight_eta.Fill(data.Phs_eta[ph])
=======
                h_num_pt.Fill(data.Phs_pt[ph]/1000.)
                h_num_eta.Fill(data.Phs_eta[ph])
    
>>>>>>> 31044d4fd4b3f271a73b5a3dd4dc76e9c8099d86:src/CodeBackUp/ComputeEff_1.py

            if IsGoodPhotonDenominator and PassSelectionCNN:
                h_numCNN_pt.Fill(data.Phs_pt[ph]/1000.)
                h_numCNN_eta.Fill(data.Phs_eta[ph])
            
    # Compute efficiencies with processed events
    Eff_Tight_pt = r.TGraphAsymmErrors()
    Eff_Tight_pt.SetName('Eff_Tight_pt')
    Eff_Tight_pt.SetTitle('Eff_Tight_pt')
    Eff_Tight_pt.Divide(h_numTight_pt,h_den_pt,'cl=0.683 b(0.5,0.5) mode')
    Eff_Tight_pt.Write()

    Eff_CNN_pt = r.TGraphAsymmErrors()
    Eff_CNN_pt.SetName('Eff_CNN_pt')
    Eff_CNN_pt.SetTitle('Eff_CNN_pt')
    Eff_CNN_pt.Divide(h_numCNN_pt,h_den_pt,'cl=0.683 b(0.5,0.5) mode')
    Eff_CNN_pt.Write()


    Eff_Tight_eta = r.TGraphAsymmErrors()
    Eff_Tight_eta.SetName('Eff_Tight_eta')
    Eff_Tight_eta.SetTitle('Eff_Tight_eta')
    Eff_Tight_eta.Divide(h_numTight_eta,h_den_eta,'cl=0.683 b(0.5,0.5) mode')
    Eff_Tight_eta.Write()

    Eff_CNN_eta = r.TGraphAsymmErrors()
    Eff_CNN_eta.SetName('Eff_CNN_eta')
    Eff_CNN_eta.SetTitle('Eff_CNN_eta')
    Eff_CNN_eta.Divide(h_numCNN_eta,h_den_eta,'cl=0.683 b(0.5,0.5) mode')
    Eff_CNN_eta.Write()

    # Plot
    c = r.TCanvas('Canvas')
    # --Tight
    Eff_Tight_pt.Draw('AZEP')
    c.SaveAs('Eff_Tight_Pt.eps')

    Eff_Tight_eta.Draw('AZEP')
    c.SaveAs('Eff_Tight_Eta.eps')
    
    # --CNN
    Eff_CNN_pt.Draw('AZEP')
    c.SaveAs('Eff_CNN_Pt.eps')

    Eff_CNN_eta.Draw('AZEP')
    c.SaveAs('Eff_CNN_Eta.eps')
>>>>>>> 31044d4fd4b3f271a73b5a3dd4dc76e9c8099d86

if __name__ == "__main__":
    main(sys.argv[1:])
