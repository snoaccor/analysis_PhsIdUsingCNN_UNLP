# == Here I store Deprecated code that might be usefull for insight ==#

#################### NOT IN USE - DEPRICATED ####################
#===============================================================#

def GetIndexOfMaxE(ClusterDict,MaxDict):
    # get the index of the maximum value of E
    for k in ClusterDict.keys():
        arr=ClusterDict[k][2]    #this gets the E array
        maxE=max(arr)
        i_maxE=np.argmax(arr)    #this gets the index of the max value in the array
        MaxDict[k]=[i_maxE,maxE]
    return MaxDict

def GetCenterCell_Dict(ClusterDict,MaxDict,CenterCellDict):
    """
    CenterCell=[etaCenter,phiCenter,ECenter] where XCenter means that that value will be in the center of the Matrix
    CenterCell=[ClustDic[key][etaArray = 0][etaCenter],phiCenter,ECenter]
    """
    
    for k in MaxDict.keys():
        index_maxE=MaxDict[k][0]
        Cluster=ClusterDict[k]
        etaCenter=Cluster[0][index_maxE]
        phiCenter=Cluster[1][index_maxE]
        ECenter=Cluster[2][index_maxE] # at the same time this is equal to Maximums[ClusterKey][1]
        # ECenter=MaxDict[k][1] 
        CenterCellDict[k]=np.asarray([etaCenter,phiCenter,ECenter])
    
    return CenterCellDict

def GetStepsToCenter(ClusterDict,CenterCellDict,StepsToCenterDict):
    """
    To map the rest of the cells to matrix we need to map them according to etaCenter & phiCenter
    We need to define minumum distance between cells, which we will call stepX; we will have one stepEta 
    and one stepPhi
    """
    
    #This values have been taken from bibliography
    stepEta=0.025
    stepPhi=0.02449

    
    for k in ClusterDict.keys():
        Cluster=ClusterDict[k]
        CenterCell=CenterCellDict[k]
        
#         CenterEta=Cluster.centerEta
#         CenterPhi=Cluster.centerPhi
        
        # --Now we need to center each eta and phi of the cluster
        CenteredCluster=[Cluster[0]-CenterCell[0],Cluster[1]-CenterCell[1],Cluster[2]]
        
        # --Because phi is the azimutal angle that lies in [-pi,pi] and there is a cut at the borders,
        # some cells get truncated. To fix this use the symmetry by substracting a pi phase
        # to those cells with a phi coord distance to de central cell greater than Pi
        
        DistToPhi=Cluster[1]-CenterCell[1]
        i=0
        for distphi in DistToPhi:
            if abs(distphi) >= np.pi:

                # fix the value
                Cluster[1][i]+=np.pi*np.sign(CenterCell[1])
               
                #replace in array
                CenteredCluster[1][i] = Cluster[1][i]
                i+=1
            else: i+=1
        
        # --CenteredCluster[0] es un array ..#we add 0.00125 so when the // acts it will round correctly
        
        STP_eta = (CenteredCluster[0]+(stepEta/2)*np.sign(CenteredCluster[0]))//stepEta
        STP_phi = (CenteredCluster[1]+(stepPhi/2)*np.sign(CenteredCluster[1]))//stepPhi
        
        
        # --since we need integer steps, and we are using np.array objects:
        StepsToCenterDict[k]=[
                STP_eta.astype(int),
                STP_phi.astype(int),
                CenteredCluster[2]
                ]    
        
    return StepsToCenterDict   

def MakeCCMd(Clusters_Data,Matrix_Xdim,Matrix_Ydim,ErrInfo=None):
    if ErrInfo == None: ErrInfo=False
    """
    Input::
        Clusters_Data: dictionary of clusters filled with [eta,phi,E] arrays.
        Matrix_dim: desired dimensions of the output matrices.
    
    Output::
        Norm_CCMd: dictionary filled with normalized cluster matrices.
    """
    # --Extract the maximum cell indexes from each cluster
    Maximums={}
    Maximums = GetIndexOfMaxE(Clusters_Data,Maximums)#Cell_Module.GetIndexOfMaxE(Clusters_Data,Maximums)
    
    # --Create a dictionary to store the center cells
    CenterCellDict={}
    CenterCellDict= GetCenterCell_Dict(Clusters_Data,Maximums,CenterCellDict) 
    
    # --Parametrize to distances to the center cells as coordinates in a matrix
    StepsToCenterDict = {}
    StepsToCenterDict=GetStepsToCenter(Clusters_Data,CenterCellDict,StepsToCenterDict)
    
    
    # --Create the CenterClusterMatrix_dictionary
    CCMd={}

    if ErrInfo == True:
        CCMd,ErrClusters=GetCCMd(StepsToCenterDict,CCMd,Matrix_Xdim,Matrix_Ydim,ErrInfo=ErrInfo)
        
    else:
        CCMd=GetCCMd(StepsToCenterDict,CCMd,Matrix_Xdim,Matrix_Ydim,ErrInfo=ErrInfo)
        
    # --Normalize each cluster matrix in the CCMd
    Norm_CCMd=NormalizeE(CCMd)
    
    # --Clear al expendable dictionaries
    Maximums.clear()
    CenterCellDict.clear()
    StepsToCenterDict.clear()
    
    # --Case/Switch returns
    if ErrInfo == True:
        return Norm_CCMd, ErrClusters
    else:
        return Norm_CCMd
    
    
# def ErrInCells(ErrCells,CusterDict,mX,mY,E):
    
#     print(
#         "for % error in cell(%s,%s),prev E: %s, new E: %s" %(
#             k,cX_Coord+row,
#             cY_Coord+col,
#             CCM[cX_Coord+row,cY_Coord+col],
#             E
#         )
#     )
#     return ErrCells
