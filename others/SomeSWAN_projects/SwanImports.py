# few definitions for my swan notebook to ease the draw of plots

import ROOT as r
#r.gROOT.LoadMacro("AtlasStyle.C") 
#r.gROOT.LoadMacro("AtlasUtils.C") 
#r.gROOT.LoadMacro("AtlasLabels.C") 

# Ugly global variables
style=[]
style=[ 20, 26, 22, 24]

color=[]
color=[1,2 ,4, 6, 8]

Pylots={}

Properties={}
Properties['XAxisName']={}
Properties['YAxisName']={}
Properties['XAxisRange']={}
Properties['YAxisRange']={}
Properties['AtlLbl']={}
Properties['TLatex']={}
Properties['Description']={}
Properties['LegendPlace']={}



def InspectF( filename, basename='', l_filter=[], debug=False):
    m_f = r.TFile(filename,'r')
    Inspect(m_f, basename, l_filter,debug)


def Inspect( TDir, basename='', l_filter=[], debug=False):
    # This function should return an array of strings containing *all* the hist names in your root file (digging within subdirectories and all)
    # It calls itself recursivelly... it seems to work...

    # TDir is a r.TDirectory
    # basename is the name of the object we want to look into
    # l_filter is a list of strings. *Only* objects with name containing partially any of the text in l_filter will be taken into account
    out = []
    if not TDir:
        print('ERROR: Can\'t open TDir, or file!')
        return out

    if debug:
        print('DEBUG::TDir     = %s'%TDir)
        print('DEBUG::basename = %s'%basename)
    try:
        if basename!='':
            TSubDirs = TDir.Get(basename).GetListOfKeys()
        else:
            TSubDirs = TDir.GetListOfKeys()
            print('Nobasename')
            print(TSubDirs)
    except:
        # OK we are at the end of the tree here.
        # if basename matches *any* of l_filter (in case l_filter is not empty), then we will return
        if len(l_filter)==0:
            return [basename]
        else:
            for l in l_filter:
                if l in basename:
                    return [basename]
            return[]


    for d in TSubDirs:
        m_Name  = d.GetName()
        if basename!='':
            subdirname = basename+'/'+ m_Name 
        else:
            subdirname = m_Name 
        
        for m_inspect in Inspect(TDir, subdirname, l_filter):
            #print(m_inspect)
            # Only print it out if ALL filters in:
            passFilter=True
            for l in l_filter:
                if l not in m_inspect:
                    passFilter=False

            if passFilter:               
                if debug:
                    print('DEBUG:: appending: %s' %m_inspect)
                out.append(m_inspect)

    return out
def SetAtlasStyle ():
  print ("\nApplying ATLAS style settings...")
  atlasStyle = AtlasStyle()
  r.gROOT.SetStyle("ATLAS")
  r.gROOT.ForceStyle()

def AtlasStyle():
  atlasStyle = r.TStyle("ATLAS","Atlas style")
  # use plain black on white colors
  icol=0 # WHITE
  atlasStyle.SetFrameBorderMode(icol)
  atlasStyle.SetFrameFillColor(icol)
  atlasStyle.SetCanvasBorderMode(icol)
  atlasStyle.SetCanvasColor(icol)
  atlasStyle.SetPadBorderMode(icol)
  atlasStyle.SetPadColor(icol)
  atlasStyle.SetStatColor(icol)
  #atlasStyle.SetFillColor(icol) # don't use: white fill color for *all* objects
  # set the paper & margin sizes
  atlasStyle.SetPaperSize(20,26)

  # set margin sizes
  atlasStyle.SetPadTopMargin(0.07)
  atlasStyle.SetPadRightMargin(0.09)
  atlasStyle.SetPadBottomMargin(0.16)
  atlasStyle.SetPadLeftMargin(0.16)

  # set title offsets (for axis label)
  atlasStyle.SetTitleXOffset(1.4)
  atlasStyle.SetTitleYOffset(1.4)

  # use large fonts
  #Int_t font=72 # Helvetica italics
  font=42 # Helvetica
  tsize=0.05
  atlasStyle.SetTextFont(font)

  atlasStyle.SetTextSize(tsize)
  atlasStyle.SetLabelFont(font,"x")
  atlasStyle.SetTitleFont(font,"x")
  atlasStyle.SetLabelFont(font,"y")
  atlasStyle.SetTitleFont(font,"y")
  atlasStyle.SetLabelFont(font,"z")
  atlasStyle.SetTitleFont(font,"z")

  atlasStyle.SetLabelSize(tsize,"x")
  atlasStyle.SetTitleSize(tsize,"x")
  atlasStyle.SetLabelSize(tsize,"y")
  # use bold lines and markers
  atlasStyle.SetMarkerStyle(20)
  atlasStyle.SetMarkerSize(1.2)
  atlasStyle.SetHistLineWidth(2)
  atlasStyle.SetLineStyleString(2,"[12 12]") # postscript dashes

  # get rid of X error bars 
  #atlasStyle.SetErrorX(0.001)
  # get rid of error bar caps
  atlasStyle.SetEndErrorSize(0.)

  # do not display any of the standard histogram decorations
  atlasStyle.SetOptTitle(0)
  #atlasStyle.SetOptStat(1111)
  atlasStyle.SetOptStat(0)
  #atlasStyle.SetOptFit(1111)
  atlasStyle.SetOptFit(0)

  # put tick marks on top and RHS of plots
  atlasStyle.SetPadTickX(1)
  atlasStyle.SetPadTickY(1)
  atlasStyle.SetPalette(1)
  return atlasStyle


def ATLASLabel(x, y, text='', color=1):
    out = []
    out.append(r.TLatex())
    out[-1].SetNDC()
    out[-1].SetTextFont(72)
    out[-1].SetTextColor(color)


    out[-1].DrawLatex(x,y,"ATLAS")

    if text:
        delx = 0.115*696*r.gPad.GetWh()/(472*r.gPad.GetWw())
        out.append(r.TLatex())
        out[-1].SetNDC()
        out[-1].SetTextFont(42)
        out[-1].SetTextColor(color)
        out[-1].DrawLatex(x+delx,y,text)
    #print(out)
    return out
   




def PlotVars(Plots, Samples, YRanges, Save=False):
    # This one will loop over samples, and for each sample it will plot all Plots
    canvases=[]
    legs=[]
    hists=[]
    files=[]
    texts=[]
    
    for sample in Samples:
        print('Sample '+str(sample))
        canvases.append(r.TCanvas(str(len(canvases)),str(len(canvases))))
        Yrange=[-1, -1]

        if sample in YRanges:
            Yrange = YRanges[sample]

        # legend
        legs.append(r.TLegend(0.5,0.35,0.70,0.20))
        legs[-1].SetBorderSize(0)
        #leg.SetTextFont(42)
        legs[-1].SetTextSize(0.04)
        legs[-1].SetLineColor(1)
        legs[-1].SetLineStyle(1)
        legs[-1].SetLineWidth(1)
        legs[-1].SetFillColor(0)
        legs[-1].SetFillStyle(0)
        order=0

        for plot in Plots:
            
            print('Plotting plot'+str(plot))
            DrawOpts='EP'
            if order>0:
                DrawOpts+='same'
            files.append(r.TFile(Samples[sample]))
            hists.append(files[-1].Get(Plots[plot]))
            if not hists[-1]:
                print('WARNING! plot for %s not found! skipping this one'%Plots[plot])
                continue

            hists[-1].SetMarkerColor(color[order])
            hists[-1].SetLineColor(color[order])
            hists[-1].SetMarkerStyle(style[order])
            if Yrange[1]>Yrange[0]:
                hists[-1].GetYaxis().SetRangeUser(Yrange[0],Yrange[1])
            hists[-1].Draw(DrawOpts)
            legs[-1].AddEntry(hists[-1],plot)
            order+=1

        legs[-1].Draw()
        
        texts.append(r.TLatex())
        texts[-1].SetNDC()
        texts[-1].SetTextFont(42)
        texts[-1].SetTextSize(0.04)
        texts[-1].SetTextColor(1)
        texts[-1].DrawLatex(0.2, 0.8,plot)


        canvases[-1].Draw()
        if Save:
            canvases[-1].SaveAs('Output/'+plot+'.eps')

        Output={}
        Output['texts'] = texts
        Output['hists'] = hists
        Output['legs'] = legs
        Output['files'] = files
        Output['canvases'] = canvases
        
    return Output

def Pylot(Pylots, Properties, Save=False):
#def Pylot(Pylots, YRanges, Save=False, Label={}, XAxisName={}, YAxisName={}, AtlLbl='')
    # Pylot-like plotting. Pylot is a dictionary containing Name as key and ["Filnema,Histname,Legend",  "Filename,Histname,Legend, ....]  as value 
    # Label is a dictionary that contains a description for each plot to be shown as title before the legends
    XAxisName={}
    YAxisName={}
    XAxisRange={}
    YAxisRange={}
    Legend={}
    AtlLbl={}
    Description={}
    TLatex={}
    LegendPlace={}

    if 'XAxisName'  in Properties:
        XAxisName    = Properties['XAxisName']
    if 'YAxisName'  in Properties:
        YAxisName    = Properties['YAxisName']
    if 'XAxisRange' in Properties:
        XAxisRange   = Properties['XAxisRange']
    if 'YAxisRange' in Properties:
        YAxisRange   = Properties['YAxisRange']
    if 'Description'     in Properties:
        Description       = Properties['Description']
    if 'AtlLbl'     in Properties:
        AtlLbl       = Properties['AtlLbl']
    if 'TLatex'     in Properties:
        TLatex       = Properties['TLatex']
    if 'LegendPlace'     in Properties:
        LegendPlace       = Properties['LegendPlace']


    
    canvases=[]
    legs=[]
    hists=[]
    files=[]
    texts=[]
    legendplaces=[0.275,0.35,0.70,0.20]
    
    for fig in Pylots:
        canvases.append(r.TCanvas(str(len(canvases)),str(len(canvases))))

        Yrange=[-1, -1]
        Xrange=[-1, -1]
        if fig in LegendPlace:
            if len(LegendPlace[fig]) == 4:
                legendplaces = LegendPlace[fig]

        lx0=legendplaces[0]
        ly0=legendplaces[1]
        lx1=legendplaces[2]
        ly1=legendplaces[3]
            

        if fig in YAxisRange:
            Yrange = YAxisRange[fig]
        if fig in XAxisRange:
            Xrange = XAxisRange[fig]
        if fig in Description:
            LegendText = Description[fig]
        else:
            try:
                LegendText = fig.split('__')[0]
            except:
                print('Ups, it seems fig does not have a name with \'__\' in it... skipping LegendText setting')
        
        print('INFO:: Setting legend to %s'%LegendText)


        Plots = Pylots[fig] # This will be an array of Filename,HistName,Legend





        # Add text of the plot
        # legend
        legs.append(r.TLegend(lx0,ly0,lx1,ly1))
        legs[-1].SetBorderSize(0)
        #legs[-1].SetTextFont(42)
        #legs[-1].SetTextSize(0.04)
        legs[-1].SetLineColor(1)
        legs[-1].SetLineStyle(1)
        legs[-1].SetLineWidth(1)
        legs[-1].SetFillColor(0)
        legs[-1].SetFillStyle(0)
        order=0

        for plot in Plots:
            #print('Plot '+str(plot))
            m_filename, m_histname, m_legend = plot.split(';')
            #print('File %s, Hist %s, legend %s'%(m_filename, m_histname, m_legend))
            files.append(r.TFile(m_filename))
            hists.append(files[-1].Get(m_histname))
            if not hists[-1]:
                print('WARNING! plot for %s not found! skipping this one'%plot)
                continue

            DrawOpts='EP'
            #Check if it is a histogram. If so, normalize it to one
            if 'TH1' in hists[-1].ClassName():
                try:
                    print('hists[-1].Scale(1./hists[-1].Integral())')
                except:
                    print('WARNING::Something weird happened scaling, likely reference hist empty.... NOT scaling')

            if order>0:
                DrawOpts+='same'
            else:
                # So if Order *is* 0 then if defined to set names for axis, do it now:
                if fig in XAxisName:
                    hists[-1].GetXaxis().SetTitle(XAxisName[fig])
                if fig in YAxisName:
                    hists[-1].GetYaxis().SetTitle(YAxisName[fig])
                if Yrange[1]>Yrange[0]:
                    hists[-1].GetYaxis().SetRangeUser(Yrange[0],Yrange[1])
                if Xrange[1]>Xrange[0]:
                    hists[-1].GetXaxis().SetRangeUser(Xrange[0],Xrange[1])
                hists[-1].GetYaxis().SetTitleSize(0.045)





            hists[-1].SetMarkerColor(color[order])
            hists[-1].SetLineColor(color[order])
            hists[-1].SetMarkerStyle(style[order])
            hists[-1].Draw(DrawOpts)
            legs[-1].AddEntry(hists[-1],m_legend)
            order+=1

        legs[-1].Draw()
        texts.append(r.TLatex())
        texts[-1].SetNDC()
        texts[-1].SetTextFont(42)
        texts[-1].SetTextSize(0.044)
        texts[-1].SetTextColor(1)
        texts[-1].DrawLatex(lx0, ly0+0.01,LegendText)
        
        if fig in TLatex:
            m_latexes = TLatex[fig]
            # this is an array of [x,y,legend]
            for m_tlatex in m_latexes:
                if len(m_tlatex) != 3:
                    print('ERROR! m_tlatex = %s is NOT of the forrm [x,y, text]'%str(m_tlatex))
                    continue

                texts.append(r.TLatex())
                texts[-1].SetNDC()
                texts[-1].SetTextFont(42)
                texts[-1].SetTextSize(0.044)
                texts[-1].SetTextColor(1)
                texts[-1].DrawLatex(float(m_tlatex[0]),float(m_tlatex[1]),m_tlatex[2])



        if fig in AtlLbl:
            print('Printing AtlLbl = %s'%AtlLbl[fig])
            texts.append(ATLASLabel(0.55, 0.85, AtlLbl[fig], 1))
        else:
            print('No AtlLbl for fig....')
            print(AtlLbl)


        canvases[-1].Draw()
        if Save:
            canvases[-1].SaveAs('Output/'+fig+'.eps')

        Output={}
        Output['texts'] = texts
        Output['hists'] = hists
        Output['legs'] = legs
        Output['files'] = files
        Output['canvases'] = canvases
        
    return Output



def Plot(Plots, Samples, YRanges, Save=False):
    # This one will loop over Plots, and for each plot will show all Samples
    canvases=[]
    legs=[]
    hists=[]
    files=[]
    texts=[]
    
    for plot in Plots:
        canvases.append(r.TCanvas(str(len(canvases)),str(len(canvases))))
        Yrange=[-1, -1]

        if plot in YRanges:
            Yrange = YRanges[plot]

        
        print('Plotting plot'+str(plot))
        # legend
        legs.append(r.TLegend(0.5,0.35,0.70,0.20))
        legs[-1].SetBorderSize(0)
        #leg.SetTextFont(42)
        legs[-1].SetTextSize(0.04)
        legs[-1].SetLineColor(1)
        legs[-1].SetLineStyle(1)
        legs[-1].SetLineWidth(1)
        legs[-1].SetFillColor(0)
        legs[-1].SetFillStyle(0)
        order=0

        for sample in Samples:
            print('Sample '+str(sample))
            DrawOpts='EP'
            if order>0:
                DrawOpts+='same'
            files.append(r.TFile(Samples[sample]))
            hists.append(files[-1].Get(Plots[plot]))
            if not hists[-1]:
                print('WARNING! plot for %s not found! skipping this one'%Plots[plot])
                continue

            hists[-1].SetMarkerColor(color[order])
            hists[-1].SetLineColor(color[order])
            hists[-1].SetMarkerStyle(style[order])
            if Yrange[1]>Yrange[0]:
                hists[-1].GetYaxis().SetRangeUser(Yrange[0],Yrange[1])
            hists[-1].Draw(DrawOpts)
            legs[-1].AddEntry(hists[-1],sample)
            order+=1

        legs[-1].Draw()
        
        texts.append(r.TLatex())
        texts[-1].SetNDC()
        texts[-1].SetTextFont(42)
        texts[-1].SetTextSize(0.04)
        texts[-1].SetTextColor(1)
        texts[-1].DrawLatex(0.2, 0.8,plot)


        canvases[-1].Draw()
        if Save:
            canvases[-1].SaveAs('Output/'+plot+'.eps')

        Output={}
        Output['texts'] = texts
        Output['hists'] = hists
        Output['legs'] = legs
        Output['files'] = files
        Output['canvases'] = canvases
        
    return Output




print('Use as')

print('SetAtlasStyle()')
print('r.gROOT.SetBatch(True)')
print('r.gStyle.SetPalette(1)')
print('')
print('Samples = {}')
print('Samples[\'r10290\'] = \'data17_13TeV.00339070.physics_EnhancedBias.merge.HIST.r10290_r10291_p2944_p2944/HIST.13212996._000001.pool.root.1\'')
print('Samples[\'r10311\'] = \'data17_13TeV.00339070.physics_EnhancedBias.merge.HIST.r10311_r10312_p2944_p2944/HIST.13294753._000001.pool.root.1\'')
print('')
print('Plots={}')
print('Plots[\'L2_et\'] = \'run_339070/HLT/Egamma/Expert/HLT_e28_lhtight_nod0_ivarloose/Efficiency/L2Calo/eff_et\'')
print('Plots[\'HLT_phi\'] = \'run_339070/HLT/Egamma/Expert/HLT_e28_lhtight_nod0_ivarloose/Efficiency/HLT/eff_phi\'')
print('')
print('# and then draw with:')
print('canvases, hists, files,legs= Plot(Plots,Samples)')

def GetThreshold(trigname):
    # Expected name such as HLT_[ge][number]_[whatever]
    result = trigname.split('_')[1].replace('g','').replace('e','')
    return int(result)
    


